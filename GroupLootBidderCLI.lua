-- Copyright (c) 2009-2011, r. brian harrison.  All rights reserved.


-- aliases

local GLB = GroupLootBidder


local function print(s)
    DEFAULT_CHAT_FRAME:AddMessage(s)
end


function GLB.Command()
    local official = GLB.IsOfficial()
    if (GLB.VERSION ~= GLB.MostRecentVersion) then
	print(string.format("Version %s (most recent %s)", GLB.VERSION, GLB.MostRecentVersion))
    else
	print(string.format("Version %s", GLB.VERSION))
    end
    if official then
	print("Official usage:")
    else
	print("Non-official usage:")
    end
    usage = {
	{ true,     "/glbalt", "", "list all alts" },
	{ true,     "/glbalt", "PLAYER", "list player's alts or main character" },
	{ official, "/glbalt", "ALT MAIN", "declare the alt of a main character" },
	{ official, "/glbalt", "ALT nil", "disassociates alt from a main character" },
	{ true,     "/glbaward", "", "list all awarded loot" },
	{ true,     "/glbaward", "PLAYER", "list all loot awarded to player" },
	{ true,     "/glbpoints", "", "list all points" },
	{ true,     "/glbpoints", "PLAYER", "list points for player" },
	{ official, "/glbpoints", "PLAYER POINTS", "sets points for player" },
	{ official, "/glbpoints", "PLAYER nil", "removes points for player" },
	{ true,     "/glbraid", "", "lists all raids" },
	{ true,     "/glbraid", "PLAYER", "lists all raids attended by player" },
	{ true,     "/glbraid", "RAID_ID_PREFIX", "lists raids by their raid id" },
	{ official, "/glbpay", "EARNED_POINTS", "gives each player in this raid some points" },
	{ official, "/glbpay", "RAID_ID EARNED_POINTS", "gives each player in a raid some points" },
	{ true,     "/glbpool", "", "lists point pools" },
	{ official, "/glbpool", "POOL_ID", "changes the point pool" },
	{ official, "/glbmanualauction", "ITEM", "opens bidding of arbitrary item" },
	{ official, "/glbmanualaward", "PLAYER LEVEL ITEM", "awards an item by hand (level can only be Full, Half, or 5pt.)" },
	{ official, "/glbmanualaward", "PLAYER LEVEL", "awards your recently awarded item by hand" },
	{ official, "/glbmanualaward", "PLAYER", "awards by hand" },
	{ official, "/glbrevoke", "ITEM", "undo most recent award of item" },
	{ official, "/glbrevoke", "", "undo your most recent awarded item" },
	{ true,     "/glbsync", "", "ask all addons to broadcast their data" },
    }
    for _, data in ipairs(usage) do
	local available, slash, args, comment = unpack(data)
	if available then
	    print(string.format("    %s |cffff00ff%s|r |cff808080%s|r",
				slash, args, comment))
	end
    end
end


function GLB.DoManualAuction(item_name_or_link)
    if item_name_or_link == "" then
	item_name_or_link = "|cffffffff|Hitem:3770:0:0:0:0:0:0:1733319661:31|h[Mutton Chop]|h|r"
    end
    if GLB.IsOfficial() then
	-- XXX whisper everyone with the addon to make sure they know the item?
	GLB.AnnounceManualAuction(GLB.ManualAuctionDuration, item_name_or_link)
    else
	GLB.ManualAuction(UnitName("player"), GLB.ManualAuctionDuration, item_name_or_link)
    end
end


function GLB.TweakPoints(msg)
    local db = GroupLootBidder_PointsDB[GroupLootBidder_PointPool]
    local bidder, points = string.match(msg, "(%S+)%s+(%S+)")
    local old_points
    if bidder == nil then
        bidder = string.match(msg, "(%S+)")
    end
    if bidder ~= nil then
        bidder = GLB.capitalize(bidder)
        old_points = GLB.GetPoints(bidder)
        if old_points == nil then
            old_points = "no"
        end
    end
    if bidder ~= nil and points == "nil" then
	if not GLB.IsOfficial() then
	    print("Not an official.")
	    return
	end
        GLB.SetPoints(bidder, nil)
    elseif bidder ~= nil and points ~= nil then
	if not GLB.IsOfficial() then
	    print("Not an official.")
	    return
	end
        GLB.SetPoints(bidder, tonumber(points))
        local _, mtime, routing = GLB.GetPoints(bidder)
        GLB.SendPointsUpdate(bidder, points, mtime, routing)
        if bidder ~= UnitName("player") then
            msg = "You now have "..points.." points."
            -- SendChatMessage(msg, "WHISPER", nil, bidder)
        end
    elseif bidder ~= nil then
	local points, mtime, routing = GLB.GetPoints(bidder)
	if routing ~= "" then
	    routing = " per "..routing
	end
	if points == nil then
	    points = "no"
	    mtime = ""
	    routing = ""
	end
        print(string.format("%s has %s points |c20333333%s%s|r", bidder, points, mtime, routing))
    else
        print("Points for "..GroupLootBidder_PointPool)
        local bidders = GLB.table_keys(db)
        table.sort(bidders)
        for _, bidder in ipairs(bidders) do
            local _, mtime, routing = GLB.GetPoints(bidder)
            if routing ~= "" then
                routing = " per "..routing
            end
            print(string.format("    %4d %s |c20333333%s%s|r", db[bidder], bidder, mtime, routing))
        end
    end
end


function GLB.ShowAwards(msg)
    -- XXX should include alts or mains?
    -- XXX should constrain to some time interval or individual?
    local when_prefix = string.match(msg, "%d+")
    local when_prefix_len = 0
    local member_name
    if when_prefix then
	when_prefix_len = string.len(when_prefix)
	print("Awards for "..GroupLootBidder_PointPool.." on "..when_prefix)
    else
	member_name = string.match(msg, "%S+")
	if member_name then
	    member_name = GLB.capitalize(member_name)
	    print("Awards for "..GroupLootBidder_PointPool.." to "..member_name)
	else
	    print("Awards for "..GroupLootBidder_PointPool)
	end
    end
    for _, award in ipairs(GroupLootBidder_AwardDB[GroupLootBidder_PointPool] or { }) do
	local show_it = true
	if (when_prefix
	    and string.sub(award.when, 1, when_prefix_len) ~= when_prefix) then
	    show_it = false
	end
	if member_name and award.bidder ~= member_name then
	    show_it = false
	end
	if show_it then
	    print(string.format("    %s %s %s %s %s", award.when, award.bidder, award.level or "?", award.points or "?", award.link or award.item))
	end
    end
end


function GLB.RevokeAward(msg)
    if not GLB.IsOfficial() then
	print("Not an official.")
	return
    end
    local item = string.match(msg, ".+")
    if not item then
	item = GLB.MostRecentItem
	if not item then
	    print("No item.")
	    return
	end
    end
    local award_db = GroupLootBidder_AwardDB[GroupLootBidder_PointPool]
    for i = #award_db, 1, -1 do
	award = award_db[i]
	if award.item == item then
	    GLB.SendRevokeAward(award.bidder, award.level,
				award.points, award.item)
	    GLB.AdjustPoints(award.bidder, award.points)
	    local points, mtime, routing = GLB.GetPoints(award.bidder)
	    GLB.SendPointsUpdate(award.bidder, points, mtime, routing)
	    break
	end
    end
end


function GLB.ManualAward(msg)
    if not GLB.IsOfficial() then
	print("Not an official.")
	return
    end
    local bidder, level, item_name_or_link = string.match(msg, "(%S+)%s+(%S+)%s+(.+)")
    if not bidder then
	item_name_or_link = GLB.MostRecentItem
	bidder, level = string.match(msg, "(%S+)%s+(%S+)")
	if not bidder then
	    level = "Full"
	    bidder = string.match(msg, "%S+")
	    if not bidder then
		print("Specify bidder, level (defaults to Full), and item (defaults to most recent item)")
		return
	    end
	end
    end
    local item_name, link = GetItemInfo(item_name_or_link)
    bidder = GLB.capitalize(bidder)
    local total_points = GLB.GetPoints(bidder)
    if not total_points then
	print(bidder.." does not have any points.")
	return
    end
    level = GLB.ConstrainLevel(bidder, level)
    local bid_points = GLB.EstimatePoints(bidder, level, total_points)
    if not bid_points then
	print("Level can be either Full, Half or 5pt.")
	return
    end
    GLB.AnnounceAward(bidder, level, bid_points, item_name, link)
end


local function print_raid_synopsis(raid_name)
    local raid_info = GroupLootBidder_RaidDB[raid_name]
    local zones = GLB.table_keys(raid_info.zones)
    zones = #zones > 0 and table.concat(zones, ", ") or "Azeroth"
    local members = GLB.table_keys(raid_info.members)
    --[[
    local points_suffix = raid_info.points and (" |cff00ff00"..raid_info.points.." points from "..raid_info.pool.."|r") or ""
    --]]
    local payouts = { }
    local points_suffix = ""
    if raid_info.payouts then
	for _, payout in pairs(raid_info.payouts) do
	    local old_points = payouts[payout.pool] or 0
	    payouts[payout.pool] = old_points + payout.points
	end
	for pool, points in pairs(payouts) do
	    points_suffix = string.format("%s, %d %s points",
					  points_suffix,
					  points, pool)
	end
	points_suffix = " |cff00ff00"..string.sub(points_suffix, 3).."|r"
    end
    print(string.format("%s to %s with %d members%s", raid_name, zones, #members, points_suffix))
end

local function print_raid_members(raid_name)
    local raid_info = GroupLootBidder_RaidDB[raid_name]
    --local zones = GLB.table_keys(raid_info.zones)
    --if #zones > 0 then
    local members = GLB.table_keys(raid_info.members)
    table.sort(members)
    local members_str = table.concat(members, ", ")
    print("    |cff808080"..members_str.."|r")
    --end
end

function GLB.ShowRaids(msg)
    local raid_names = GLB.table_keys(GroupLootBidder_RaidDB)
    table.sort(raid_names)
    local member_name
    local main_name
    local raid_id_prefix = string.match(msg, "%d+")
    local raid_id_prefix_len = 0
    if raid_id_prefix then
	raid_id_prefix_len = string.len(raid_id_prefix)
    else
	member_name = string.match(msg, "%S+")
	if member_name then
	    member_name = GLB.capitalize(member_name)
	    main_name = GLB.Main(member_name)
	end
    end
    for _, raid_name in ipairs(raid_names) do
	local show_synopsis = true
	local show_members = true
	if member_name then
	    show_members = false
	    local raid_info = GroupLootBidder_RaidDB[raid_name]
	    -- XXX does not show alts of alts
	    if not (raid_info.members[member_name]
		    or raid_info.members[main_name]) then
		show_synopsis = false
	    end
	end
	if raid_id_prefix then
	    if not (string.sub(raid_name, 1, raid_id_prefix_len) == raid_id_prefix) then
		show_synopsis = false
	    end
	end
	if show_synopsis then
	    print_raid_synopsis(raid_name)
	    if show_members then
		print_raid_members(raid_name)
	    end
	end
    end
end


function GLB.PayRaiders(msg)
    local raid_name, income = string.match(msg, "(%S+)%s+(-?%d+)")
    if not raid_name then
	-- default to this raid
	-- XXX unsure about this shortcut
	raid_name = GroupLootBidder_CurrentRaidName
	income = string.match(msg, "(-?%d+)")
	-- HACK
	GLB.UpdateRaidMembers()
    end
    local raid_info = GroupLootBidder_RaidDB[raid_name]
    if income == nil then
	return GLB.ShowRaids("")
    elseif raid_info then
	if not GLB.IsOfficial() then
	    print("Not an official.")
	    return
	end
	income = tonumber(income)
	if raid_name and income then
	    local paid_mains = { }
	    local members = GLB.table_keys(raid_info.members)
	    table.sort(members)
	    for _, member in ipairs(members) do
		local main = GLB.Main(member)
		if not paid_mains[main] then
		    -- only pay them once
		    local old_points = GLB.GetPoints(main) or 0
		    GLB.SetPoints(main, old_points + income)
		    local points, mtime, routing = GLB.GetPoints(main)
		    GLB.SendPointsUpdate(main, points, mtime, routing)
		    paid_mains[main] = true
		end
	    end
	    local payout_id = #raid_info.payouts
	    GLB.AnnouncePayout(raid_name, GroupLootBidder_PointPool,
			       payout_id, UnitName("player"), income,
			       GLB.GetTime())
	end
    else
	print("Unknown raid")
    end
end


local warned_about_pool_typos

function GLB.TweakPool(msg)
    -- XXX do we need per-item pools?
    local new_pool = string.match(msg, "(%S+)")
    if new_pool then
	if not GLB.IsOfficial() then
	    print("Not an official.")
	    return
	end
	if not GroupLootBidder_PointsDB[new_pool] and warned_about_pool_typos ~= new_pool then
	    print("Use the name of an existing pool, or try again.")
	    warned_about_pool_typos = new_pool
	else
	    GLB.AnnouncePool(new_pool)
	end
    else
	local pools = GLB.table_keys(GroupLootBidder_PointsDB)
	print("Current pool is "..GroupLootBidder_PointPool..".  Available pools are:")
	for _, pool in ipairs(pools) do
	    print("    "..pool)
	end
    end
end


function GLB.TweakAlt(msg)
    local alt, main = string.match(msg, "(%S+)%s+(%S+)")
    if alt then
	if not GLB.IsOfficial() then
	    print("Not an official.")
	    return
	end
	alt = GLB.capitalize(alt)
	if main == "nil" then
	    main = nil
	else
	    main = GLB.capitalize(main)
	end
	GLB.SetAlt(alt, main)
	local _, mtime, routing = GLB.GetAlt(alt)
	GLB.SendAltUpdate(alt, main, mtime, routing)
    else
	local realm_alt_db = GroupLootBidder_AltDB[GetRealmName()] or { }
	local alt = string.match(msg, "(%S+)")
	if alt then
	    print("Alts for "..alt.." on "..GetRealmName()..":")
	    alt = GLB.capitalize(alt)
	    for a in pairs(realm_alt_db) do
		local main, mtime, routing = GLB.GetAlt(a)
		if a == alt or main == alt then
		    if routing ~= "" then
			routing = " per "..routing
		    end
		    print(string.format("    %s %s |c20333333%s%s|r", a, main, mtime, routing))
		end
	    end
	else
	    print("Alts on "..GetRealmName()..":")
	    for a in pairs(realm_alt_db) do
		local main, mtime, routing = GLB.GetAlt(a)
		if routing ~= "" then
		    routing = " per "..routing
		end
		print(string.format("    %s %s |c20333333%s%s|r", a, main, mtime, routing))
	    end
	end
    end
end


function GLB.Sync(msg)
    -- XXX using the same point pool?
    for player in pairs(GroupLootBidder_PointsDB[GroupLootBidder_PointPool]) do
	GLB.SendPointsRequest(player)
    end
end


function GLB.ShowAudit(msg)
    local bidder = string.match(msg, "(%S+)")
    if not bidder then
	bidder = UnitName("player")
    end
    bidder = GLB.capitalize(bidder)
    for _, row in pairs(GLB.Audit(bidder)) do
	--print(string.format("%s   %d -> %d   %s", row[1], row[5], row[4], row[2]))
	print(string.format("%s   %d -> %d   %s", row.when, row.points, row.points + row.delta, row.description))
    end
end


function GLB.ToggleDebug(msg)
    if GroupLootBidder_Debugging then
        GLB.Debug("debugging off")
	GroupLootBidder_Debugging = false
    else
        GroupLootBidder_DebugLog = GroupLootBidder_DebugLog.."--------\n"
	GroupLootBidder_Debugging = true
        GLB.Debug("debugging on")
    end
end


SLASH_GROUPLOOTBIDDER1 = "/grouplootbidder"
SLASH_GROUPLOOTBIDDER2 = "/grouplootbid"
SLASH_GROUPLOOTBIDDER3 = "/glb"
SlashCmdList["GROUPLOOTBIDDER"] = GLB.Command

SLASH_GROUPLOOTBIDDERMANUALAUCTION1 = "/glbmanualauction"
SLASH_GROUPLOOTBIDDERMANUALAUCTION2 = "/glbt"
SlashCmdList["GROUPLOOTBIDDERMANUALAUCTION"] = GLB.DoManualAuction

SLASH_GROUPLOOTBIDDERPOINTS1 = "/glbpoint"
SLASH_GROUPLOOTBIDDERPOINTS2 = "/glbpoints"
SLASH_GROUPLOOTBIDDERPOINTS3 = "/globpoint"
SLASH_GROUPLOOTBIDDERPOINTS4 = "/globpoints"
SLASH_GROUPLOOTBIDDERPOINTS5 = "/glbdkp"
SLASH_GROUPLOOTBIDDERPOINTS6 = "/glbp"
SlashCmdList["GROUPLOOTBIDDERPOINTS"] = GLB.TweakPoints

SLASH_GROUPLOOTBIDDERAWARDS1 = "/glbaward"
SLASH_GROUPLOOTBIDDERAWARDS2 = "/glbawards"
SLASH_GROUPLOOTBIDDERAWARDS3 = "/glba"
SlashCmdList["GROUPLOOTBIDDERAWARDS"] = GLB.ShowAwards

SLASH_GROUPLOOTBIDDERUNAWARD1 = "/glbrevoke"
SLASH_GROUPLOOTBIDDERUNAWARD2 = "/glbundo"
SlashCmdList["GROUPLOOTBIDDERUNAWARD"] = GLB.RevokeAward

SLASH_GROUPLOOTBIDDERMANUALAWARD1 = "/glbmanualaward"
SLASH_GROUPLOOTBIDDERMANUALAWARD2 = "/glbmanual"
SLASH_GROUPLOOTBIDDERMANUALAWARD3 = "/glbma"
SlashCmdList["GROUPLOOTBIDDERMANUALAWARD"] = GLB.ManualAward

SLASH_GROUPLOOTBIDDERPAY1 = "/glbpay"
SLASH_GROUPLOOTBIDDERPAY2 = "/glbpayout"
SlashCmdList["GROUPLOOTBIDDERPAY"] = GLB.PayRaiders

SLASH_GROUPLOOTBIDDERRAID1 = "/glbraid"
SLASH_GROUPLOOTBIDDERRAID2 = "/glbraids"
SLASH_GROUPLOOTBIDDERRAID3 = "/glbr"
SlashCmdList["GROUPLOOTBIDDERRAID"] = GLB.ShowRaids

SLASH_GROUPLOOTBIDDERPOOL1 = "/glbpool"
SlashCmdList["GROUPLOOTBIDDERPOOL"] = GLB.TweakPool

SLASH_GROUPLOOTBIDDERALT1 = "/glbalt"
SlashCmdList["GROUPLOOTBIDDERALT"] = GLB.TweakAlt

SLASH_GROUPLOOTBIDDERAUDIT1 = "/glbaudit"
SlashCmdList["GROUPLOOTBIDDERAUDIT"] = GLB.ShowAudit

SLASH_GROUPLOOTBIDDERSYNC1 = "/glbsync"
SlashCmdList["GROUPLOOTBIDDERSYNC"] = GLB.Sync

SLASH_GROUPLOOTBIDDERDEBUG1 = "/glbdebug"
SLASH_GROUPLOOTBIDDERDEBUG2 = "/glbd"
SlashCmdList["GROUPLOOTBIDDERDEBUG"] = GLB.ToggleDebug
