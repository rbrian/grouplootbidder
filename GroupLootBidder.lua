-- Copyright (c) 2009-2011, r. brian harrison.  All rights reserved.

-- high priority
-- TODO master on-off switch

-- medium priority
-- TODO log loots
-- TODO better dkp point management
-- TODO cancel a bid by passing?
-- TODO add Award pull down menu item (allowing explicit ordering override)
-- TODO remove bid sends out a "cancel" event (to remove checkmark)

-- low priority
-- TODO line wrap whispers in tooltip?
-- TODO add bid taker's whispers to a bidder's in tooltip?
-- TODO colorize whispers menu
-- TODO column resizing?
-- TODO include point pool name in whisper
-- TODO Award disasbles Warn and Award?  Revert button?
-- TODO bidder users guide
-- TODO bid taker users guide


GroupLootBidder = GroupLootBidder or { }
local GLB = GroupLootBidder


GroupLootBidder_PointPool = "None"
GroupLootBidder_PointsDB = { [GroupLootBidder_PointPool] = { } }
GroupLootBidder_PointsDB_mtime = { [GroupLootBidder_PointPool] = { } }
GroupLootBidder_PointsDB_routing = { [GroupLootBidder_PointPool] = { } }
GroupLootBidder_AwardDB = { [GroupLootBidder_PointPool] = { } }
GroupLootBidder_RaidDB = { }
GroupLootBidder_AltDB = { }
GroupLootBidder_AltDB_mtime = { }
GroupLootBidder_AltDB_routing = { }
GroupLootBidder_Debugging = false
GroupLootBidder_DebugLog = ""
GroupLootBidder_CurrentRaidName = nil

GLB.AddonIdent = "GroupLootBidder" --for SendAddonMessage() and whispers
GLB.EnableForParty = false
GLB.EnableForSolo = false
GLB.AllInPartyCanTakeBids = false
GLB.RollWarningWhileBidding = true
GLB.WhisperPrefix = "GroupLootBidder: "
GLB.BidInstructions = 'Whisper me exactly "big", "small", or "five" to place a bid, or "versus" to see eligible competitors, or "who" to list active bid takers.'
GLB.AntidisenchantWarning = 'Whisper me "small" or "five" instead.'
GLB.MostRecentVersion = GLB.VERSION
GLB.MostRecentItem = nil
GLB.PointlessRaidExpiration = 28 --days
GLB.MinimumBidPoints = 5
GLB.ManualAuctionDuration = 600
GLB.BidsCountdownStart = 45
GLB.BidsCountdownWarnings = { 30, 15 }
GLB.PendingLoots = { }


-- would like to outsource
local class_skills = {
    ["DEATHKNIGHT"] = {
        ["One-Handed Axes"] = true,
        ["One-Handed Maces"] = true,
        ["Polearms"] = true,
        ["One-Handed Swords"] = true,
        ["Two-Handed Axes"] = true,
        ["Two-Handed Maces"] = true,
        ["Two-Handed Swords"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Mail"] = true,
        ["Plate"] = true,
        ["Sigil"] = true,
    },
    ["DRUID"] = {
        ["Daggers"] = true,
        ["Fist Weapons"] = true,
        ["One-Handed Maces"] = true,
        ["Two-Handed Maces"] = true,
        ["Staves"] = true,
        ["Polearms"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Idols"] = true,
    },
    ["HUNTER"] = {
        ["Daggers"] = true,
        ["Fist Weapons"] = true,
        ["One-Handed Axes"] = true,
        ["Two-Handed Axes"] = true,
        ["One-Handed Swords"] = true,
        ["Two-Handed Swords"] = true,
        ["Polearms"] = true,
        ["Staves"] = true,
        ["Bows"] = true,
        ["Crossbows"] = true,
        ["Guns"] = true,
        ["Thrown"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Mail"] = true,
    },
    ["MAGE"] = {
        ["Daggers"] = true,
        ["One-Handed Swords"] = true,
        ["Staves"] = true,
        ["Wands"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
    },
    ["PALADIN"] = {
        ["One-Handed Axes"] = true,
        ["Two-Handed Axes"] = true,
        ["One-Handed Maces"] = true,
        ["Two-Handed Maces"] = true,
        ["One-Handed Swords"] = true,
        ["Two-Handed Swords"] = true,
        ["Polearms"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Mail"] = true,
        ["Plate"] = true,
        ["Shields"] = true,
        ["Librams"] = true,
    },
    ["PRIEST"] = {
        ["Daggers"] = true,
        ["One-Handed Maces"] = true,
        ["Staves"] = true,
        ["Wands"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
    },
    ["ROGUE"] = {
        ["One-Handed Axes"] = true,
        ["Daggers"] = true,
        ["Fist Weapons"] = true,
        ["One-Handed Maces"] = true,
        ["One-Handed Swords"] = true,
        ["Bows"] = true,
        ["Crossbows"] = true,
        ["Guns"] = true,
        ["Thrown"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
    },
    ["SHAMAN"] = {
        ["Daggers"] = true,
        ["Fist Weapons"] = true,
        ["One-Handed Axes"] = true,
        ["Two-Handed Axes"] = true,
        ["One-Handed Maces"] = true,
        ["Two-Handed Maces"] = true,
        ["Staves"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Mail"] = true,
        ["Shields"] = true,
        ["Totems"] = true,
    },
    ["WARLOCK"] = {
        ["Daggers"] = true,
        ["One-Handed Swords"] = true,
        ["Staves"] = true,
        ["Wands"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
    },
    ["WARRIOR"] = {
        ["Daggers"] = true,
        ["Fist Weapons"] = true,
        ["One-Handed Axes"] = true,
        ["Two-Handed Axes"] = true,
        ["One-Handed Maces"] = true,
        ["Two-Handed Maces"] = true,
        ["One-Handed Swords"] = true,
        ["Two-Handed Swords"] = true,
        ["Polearms"] = true,
        ["Staves"] = true,
        ["Bows"] = true,
        ["Crossbows"] = true,
        ["Guns"] = true,
        ["Thrown"] = true,
        ["Fishing Poles"] = true,
        ["Miscellaneous"] = true,
        ["Cloth"] = true,
        ["Leather"] = true,
        ["Mail"] = true,
        ["Plate"] = true,
        ["Shields"] = true,
    },
}


function GLB.capitalize(s)
    if s == "" then
	return ""
    else
	-- the first letter may be Unicode
	local first, rest = string.match(s, "^(.[\128-\193]?)(.*)")
	return string.upper(first)..string.lower(rest)
    end
end



function GLB.table_keys(t, out_table)
    if not out_table then
	out_table = { }
    end
    for k in pairs(t) do
	table.insert(out_table, k)
    end
    return out_table
end


function GLB.VersionCompare(v1, v2)
    if v1 == v2 then
	return 0
    end
    local prefix1 = string.match(v1, "^%d+")
    local prefix2 = string.match(v2, "^%d+")
    if prefix1 and prefix2 then
	if prefix1 == prefix2 then
	    -- has the interesting side effect that 1.0a == 1.0.a
	    return GLB.VersionCompare(string.match(v1, "^%d+%.?(.*)"),
				      string.match(v2, "^%d+%.?(.*)"))
	end
	return tonumber(prefix1) - tonumber(prefix2)
    elseif prefix1 then
	return 1
    elseif prefix2 then
	return -1
    elseif v1 < v2 then
	return -1
    else
	return 1
    end
end


local CalendarGetMonth_fixed = false
function CalendarGetMonth_bugfix()
    -- to fix CalendarGetMonth() from returning 2004-11 in rare situations
    if not CalendarGetMonth_fixed and CalendarFrame and CalendarFrame:IsShown() then
	local _, month, _, year = CalendarGetDate()
	CalendarSetAbsMonth(month, year)
	CalendarGetMonth_fixed = true
    end
end


function GLB.GetTime(delta)
    -- XXX works ok for small values of delta
    local _, month, day, year = CalendarGetDate()
    delta = delta or 0
    local days
    local month_offset = 0
    day = day + delta
    while true do
	CalendarGetMonth_bugfix()
	month, year, days = CalendarGetMonth(month_offset)
	if day < 1 then
	    month_offset = month_offset - 1
	    month, year, days = CalendarGetMonth(month_offset)
	    day = day + days
	elseif day > days then
	    month_offset = month_offset + 1
	    day = day - days
	else
	    break
	end
    end
    local hour, minute = GetGameTime()
    return string.format("%04d%02d%02d%02d%02d", year, month, day, hour, minute)
end


function GLB.ChatMessage(msg)
    local c = "|cff80c000"
    msg = string.gsub(msg, "|r", c)
    DEFAULT_CHAT_FRAME:AddMessage(string.format("%s%s: %s|r",
						c, GLB.AddonIdent, msg))
end


function GLB.ChatWarning(msg)
    local c = "|cffc0c000"
    msg = string.gsub(msg, "|r", c)
    DEFAULT_CHAT_FRAME:AddMessage(string.format("%s%s warning: %s|r",
						c, GLB.AddonIdent, msg))
end


function GLB.Debug(msg)
    if GroupLootBidder_Debugging then
	local c = "|cff808000"
	msg = string.gsub(msg, "|r", c)
	DEFAULT_CHAT_FRAME:AddMessage(string.format("%s%s debug: %s%s|r",
						    c, GLB.AddonIdent, msg, c))
	-- XXX danger: grows without bound
	GroupLootBidder_DebugLog = GroupLootBidder_DebugLog..string.format("%.3f %s", GetTime(), msg).."\n"
    end
end


function GLB.IsEquippableItemBy(link, unit_class)
    if link ~= nil then
	local _, _, _, _, _, item_type, item_subtype = GetItemInfo(link)
	if item_subtype ~= nil then
	    return class_skills[unit_class][item_subtype]
	end
    end
    return false
end


function GLB.GetAlt(player)
    local realm = GetRealmName()
    local main, mtime, routing
    if GroupLootBidder_AltDB[realm] then
	main = GroupLootBidder_AltDB[realm][player]
	mtime = GroupLootBidder_AltDB_mtime[realm][player]
	routing = GroupLootBidder_AltDB_routing[realm][player]
    end
    return main, (mtime or "000000000000"), (routing or "")
end


function GLB.SetPool(pool)
    GroupLootBidder_PointPool = pool
    if not GroupLootBidder_PointsDB[pool] then
	GroupLootBidder_PointsDB[pool] = { }
	GroupLootBidder_PointsDB_mtime[pool] = { }
	GroupLootBidder_PointsDB_routing[pool] = { }
	GroupLootBidder_AwardDB[pool] = { }
    end
end

function GLB.Main(bidder)
    return (GLB.GetAlt(bidder)) or bidder
end


function GLB.GetPoints(bidder)
    bidder = GLB.Main(bidder)
    local points = GroupLootBidder_PointsDB[GroupLootBidder_PointPool][bidder]
    local mtime = (GroupLootBidder_PointsDB_mtime[GroupLootBidder_PointPool][bidder] or "000000000000")
    local routing = GroupLootBidder_PointsDB_routing[GroupLootBidder_PointPool][bidder] or ""
    return points, mtime, routing
end


function GLB.SetPoints(bidder, points, mtime, routing)
    bidder = GLB.Main(bidder)
    if not mtime then
	mtime = GLB.GetTime()
    end
    if not routing then
	routing = ""
    end
    if not points then
	mtime = nil
	routing = nil
    end
    local old_points = GroupLootBidder_PointsDB[GroupLootBidder_PointPool][bidder]
    GroupLootBidder_PointsDB[GroupLootBidder_PointPool][bidder] = points
    GroupLootBidder_PointsDB_mtime[GroupLootBidder_PointPool][bidder] = mtime
    GroupLootBidder_PointsDB_routing[GroupLootBidder_PointPool][bidder] = routing
    if points ~= old_points then
	-- XXX could be spammy, especially at payout
	if routing == "" then
	    routing = "me"
	end
	GLB.ChatMessage(string.format("%s now has %s %s points (had %s) per %s",
				      bidder, (points or "none"),
				      GroupLootBidder_PointPool,
				      (old_points or "none"),
				      (routing or "?")))
    end
end


function GLB.StringStartsWith(str, prefix)
    return string.sub(str, 1, string.len(prefix)) == prefix
end


function GLB.StringEndsWith(str, suffix)
    if suffix == "" then
	return true
    end
    return string.sub(str, -string.len(suffix)) == suffix
end


function GLB.Better(value, mtime, routing, my_value, my_mtime, my_routing)
    local newer = my_mtime < mtime
    local older = my_mtime > mtime
    local equivalent = my_value == value
    local closer = GLB.StringEndsWith(my_routing, "."..routing)
    return not older and (newer or closer or not equivalent)
end


function GLB.SetPointsIfBetter(bidder, points, mtime, routing)
    local my_points, my_mtime, my_routing = GLB.GetPoints(bidder)
    if GLB.Better(points, mtime, routing, my_points, my_mtime, my_routing) then
	GLB.SetPoints(bidder, points, mtime, routing)
    end
end


function GLB.AdjustPoints(bidder, delta)
    local points = GLB.GetPoints(bidder)
    return GLB.SetPoints(bidder, (points or 0) + delta)
end


function GLB.SetAlt(alt, main, mtime, routing)
    local realm = GetRealmName()
    if not mtime then
	mtime = GLB.GetTime()
    end
    if not routing then
	routing = ""
    end
    local realm_alt_db = GroupLootBidder_AltDB[realm]
    if not realm_alt_db then
	realm_alt_db = { }
	GroupLootBidder_AltDB[realm] = realm_alt_db
	GroupLootBidder_AltDB_mtime[realm] = { }
	GroupLootBidder_AltDB_routing[realm] = { }
    end
    if main then
	-- a main cannot be an alt
	realm_alt_db[main] = nil
	GroupLootBidder_AltDB_mtime[realm][main] = mtime
	GroupLootBidder_AltDB_routing[realm][main] = routing
    end
    if realm_alt_db[alt] ~= main then
	realm_alt_db[alt] = main
	GroupLootBidder_AltDB_mtime[realm][alt] = mtime
	GroupLootBidder_AltDB_routing[realm][alt] = routing
	-- no longer keep the Alt's copy of points data/metadata around
	GroupLootBidder_PointsDB[GroupLootBidder_PointPool][alt] = nil
	GroupLootBidder_PointsDB_mtime[GroupLootBidder_PointPool][alt] = nil
	GroupLootBidder_PointsDB_routing[GroupLootBidder_PointPool][alt] = nil
	if routing == "" then
	    routing = "me"
	end
	GLB.ChatMessage(string.format("Character %s is an alt of %s per %s",
				      alt, (main or "nobody"), routing))
    end
end


function GLB.SetAltIfBetter(alt, main, mtime, routing)
    local my_main, my_mtime, my_routing = GLB.GetAlt(alt)
    if GLB.Better(main, mtime, routing, my_main, my_mtime, my_routing) then
	GLB.SetAlt(alt, main, mtime, routing)
    end
end


function GLB.Alert(msg, ...)
    -- announce a message for human consumption
    if GetNumRaidMembers() > 0 then
	if IsRaidOfficer() then
	    SendChatMessage(msg, "RAID_WARNING")
	else
	    SendChatMessage(msg, "RAID")
	end
    elseif GetNumPartyMembers() > 0 and GLB.EnableForParty then
	SendChatMessage(msg, "PARTY")
    elseif GLB.EnableForSolo then
	-- for debugging
	SendChatMessage(GLB.WhisperPrefix..msg, "WHISPER", nil, UnitName("player"))
    end
end


local function send_addon_whisper(msg, target)
    SendAddonMessage(GLB.AddonIdent, msg, "WHISPER", target)
end


local function send_addon_broadcast(msg)
    if GetNumRaidMembers() > 0 then
	SendAddonMessage(GLB.AddonIdent, msg, "RAID")
    elseif GetNumPartyMembers() > 0 and GLB.EnableForParty then
	SendAddonMessage(GLB.AddonIdent, msg, "PARTY")
    elseif GLB.EnableForSolo then
	-- for debugging
	SendAddonMessage(GLB.AddonIdent, msg, "WHISPER", UnitName("player"))
    end
end


-- network protocol

-- XXX should embed in each packet
function GLB.AnnounceVersion()
    send_addon_broadcast("version:"..GLB.VERSION)
end


function GLB.DecodeVersion(msg, sender)
    local version = string.match(msg, "^version:(.+)")
    return version
end


-- XXX should embed in each packet
function GLB.AnnounceRaid(raid_name)
    send_addon_broadcast("raid:"..raid_name)
end


function GLB.DecodeRaid(msg, sender)
    local raid_name = string.match(msg, "^raid:(.+)")
    return raid_name
end


-- XXX should embed in each packet
function GLB.AnnouncePool(pool)
    send_addon_broadcast("pool:"..pool)
end


function GLB.DecodePool(msg, sender)
    local pool = string.match(msg, "^pool:(.+)")
    return pool
end


function GLB.AnnounceManualAuction(duration, item_name)
    send_addon_broadcast(string.format("manual:%d:%s",
				       duration, item_name))
end


function GLB.DecodeManualAuction(msg, sender)
    local duration, item_name = string.match(msg, "^manual:([0-9]+):(.+)")
    return duration, item_name
end


function GLB.AnnounceAboutToTakeBids(item)
    send_addon_broadcast("aboutto:"..item)
end


function GLB.DecodeAboutToTakeBids(msg, sender)
    local item = string.match(msg, "^aboutto:(.+)")
    return item
end


function GLB.SendGoAhead(bid_taker, item)
    send_addon_whisper("goahead:"..item, bid_taker)
end


function GLB.DecodeGoAhead(msg, sender)
    local item = string.match(msg, "^goahead:(.+)")
    return item
end


function GLB.AnnounceTakingBids(item)
    send_addon_broadcast("taking:"..item)
end


function GLB.DecodeTakingBids(msg, sender)
    local item = string.match(msg, "^taking:(.+)")
    return item
end


function GLB.AnnounceCloseBidding(item)
    send_addon_broadcast("donetaking:"..item)
end


function GLB.DecodeCloseBidding(msg, sender)
    local item = string.match(msg, "^donetaking:(.+)")
    return item
end


function GLB.SendPlaceBid(level, item, bid_taker)
    send_addon_whisper(string.format("bid:%s:%s", level, item), bid_taker)
end


function GLB.DecodePlaceBid(msg, sender)
    local level, item = string.match(msg, "^bid:([^:]+):(.+)")
    return level, item
end


function GLB.SendAcceptBid(level, item, bidder, points)
    -- XXX points ignored
    send_addon_whisper(string.format("accepted:%s:%s", level, item), bidder)
end


function GLB.DecodeAcceptBid(msg, sender)
    local level, item = string.match(msg, "^accepted:([^:]+):(.+)")
    return level, item
end


-- XXX confusing name
function GLB.SendReceiveBid(target, bidder, level, points, item)
    send_addon_whisper(string.format("goodbid:%s:%s:%s:%s",
				     bidder, level, points, item), target)
end


function GLB.DecodeReceiveBid(msg, sender)
    local bidder, level, points, item = string.match(msg, "^goodbid:([^:]+):([^:]+):([^:]+):(.+)$")
    return bidder, level, points, item
end


function GLB.SendComment(target, bidder, item_name, comment)
    send_addon_whisper(string.format("comment:%s:%s:%s",
				     bidder, item_name, comment), target)
end


function GLB.DecodeComment(msg, sender)
    local bidder, item_name, comment = string.match(msg, "^comment:([^:]+):([^:]+):(.*)$")
    return bidder, item_name, comment
end


function GLB.SendCancelBid(target, bidder, item)
    send_addon_whisper(string.format("cancel:%s:%s", bidder, item), target)
end


function GLB.DecodeCancelBid(msg, sender)
    local bidder, item_name = string.match(msg, "^cancel:([^:]+):(.*)$")
    return bidder, item_name
end


function GLB.SendPointsRequest(bidder)
    -- XXX should send time-of-last-change for conflict resolution
    send_addon_broadcast("getpoints:"..bidder)
end


function GLB.DecodePointsRequest(msg, sender)
    -- XXX should send time-of-last-change for conflict resolution
    local bidder = string.match(msg, "^getpoints:([^:]+)")
    return bidder
end


function GLB.SendPointsUpdate(bidder, points, mtime, routing)
    if points then
	send_addon_broadcast(string.format("points:%s:%s:%s:%s",
					   bidder, points, mtime, routing))
    end
end


function GLB.DecodePointsUpdate(msg, sender)
    local bidder, points, mtime, routing = string.match(msg, "^points:([^:]+):(%-?%d+):(%d+):(.*)")
    if bidder then
	points = tonumber(points)
	if routing ~= "" then
	    routing = sender.."."..routing
	else
	    routing = sender
	end
    end
    return bidder, points, mtime, routing
end


function GLB.AnnounceDisenchant(item_name)
    send_addon_broadcast("disenchant:"..item_name)
end


function GLB.DecodeDisenchant(msg, sender)
    local item_name = string.match(msg, "^disenchant:([^:]+)$")
    return item_name
end


function GLB.AnnounceAward(bidder, level, points, item, link)
    send_addon_broadcast(string.format("award:%s:%s:%s:%s:%s:%s",
				       GLB.GetTime(),
				       bidder, level, points, item, link))
end


function GLB.DecodeAward(msg, sender)
    local when, bidder, level, points, item, link = string.match(msg, "^award:([^:]+):([^:]+):([^:]*):([^:]*):([^:]+):(.+)$")
    if not bidder then
	-- backwards compatability
	bidder, level, points, item, link = string.match(msg, "^award:([^:]+):([^:]*):([^:]*):([^:]+):(.+)$")
	if bidder then
	    when = GLB.GetTime()
	end
    end
    return when, bidder, level, points, item, link
end


function GLB.AnnounceBiddingDone(item)
    send_addon_broadcast("donebidding:"..item)
end


function GLB.DecodeBiddingDone(msg, sender)
    local item = string.match(msg, "^donebidding:(.+)")
    return item
end


function GLB.SendRevokeAward(bidder, level, points, item)
    send_addon_broadcast(string.format("unaward:%s:%s:%s:%s",
				       bidder, level, points, item))
end


function GLB.DecodeRevokeAward(msg, sender)
    local bidder, level, points, item = string.match(msg, "^unaward:([^:]+):([^:]*):([^:]*):(.+)")
    return bidder, level, points, item
end


function GLB.SendAltUpdate(alt, main, mtime, routing)
    if not main then
	main = ""
    end
    send_addon_broadcast(string.format("alt:%s:%s:%s:%s",
				       alt, main, mtime, routing))
end


function GLB.DecodeAltUpdate(msg, sender)
    local alt, main, mtime, routing = string.match(msg, "^alt:([^:]+):([^:]*):(%d+):(.*)$")
    if alt then
	if main == "" then
	    main = nil
	end
	if routing ~= "" then
	    routing = sender.."."..routing
	else
	    routing = sender
	end
    end
    return alt, main, mtime, routing
end


function GLB.AnnouncePayout(raid_name, pool, payout_id, payer, earned, when)
    -- XXX does NOT adjust player points yet
    send_addon_broadcast(string.format("payout:%s:%s:%s:%s:%d:%s",
				       raid_name, pool,
				       payout_id, payer, earned, when))
end


function GLB.DecodePayout(msg, sender)
    local raid_name, pool, payout_id, payer, earned, when = string.match(msg, "^payout:([^:]+):([^:]+):([^:]+):([^:]+):([0-9]+):([^:]+)$")
    return raid_name, pool, payout_id, payer, earned, when
end


function GLB.SendRaidNamesRequest(target, before)
    send_addon_whisper(string.format("getraidnames:%s", before), target)
end


function GLB.DecodeOldRaidNamesRequest(msg, sender)
    local before = string.match(msg, "^getraidnames:([^:]+)$")
    return before
end


function GLB.SendOldRaidName(target, raid_name)
    if raid_name then
	send_addon_whisper(string.format("oldraid:%s", raid_name), target)
    else
	send_addon_whisper("oldraiddone", target)
    end
end


function GLB.DecodeOldRaidName(msg, sender)
    local raid_name = string.match(msg, "^oldraid:([^:]+)$")
    return raid_name
end


function GLB.DecodeOldRaidNamesDone(msg, sender)
    return msg == "oldraiddone"
end


function GLB.SendRaidDBRequest(target, raid_name)
    send_addon_whisper(string.format("getraidinfo:%s", raid_name), target)
end


function GLB.DecodeRaidDBRequest(msg, sender)
    local raid_name = string.match(msg, "^getraidinfo:([^:]+)$")
    return raid_name
end


function GLB.SendRaidDB(target, raid_name, members, payouts)
    send_addon_whisper(string.format("raidinfo:%s:%s:%s", raid_name, members, payouts), target)
end


function GLB.DecodeRaidDB(msg, sender)
    local raid_name, members, payouts = string.match(msg, "^raidinfo:([^:]+):([^:]*):([^:]*)$")
    return raid_name, members, payouts
end


function GLB.SendAwardDBRequest(target, pool, index)
    send_addon_whisper(string.format("getawarded:%s:%d", pool, index), target)
end


function GLB.DecodeAwardDBRequest(msg, sender)
    local pool, index = string.match(msg, "^getawarded:([^:]+):([0-9]+)$")
    return pool, tonumber(index)
end


function GLB.SendAwardDB(target, pool, index, when, bidder, level, points, item, bidtaker, zone, link)
    if bidder then
	send_addon_whisper(string.format("awarded:%s:%d:%s:%s:%s:%s:%s:%s:%s:%s", pool, index, when, bidder, level, points, item, bidtaker, zone, link), target)
    else
	send_addon_whisper(string.format("awardeddone:%s:%d", pool, index), target)
    end
end


function GLB.DecodeAwardDB(msg, sender)
    local pool, index, when, bidder, level, points, item, bidtaker, zone, link = string.match(msg, "^awarded:([^:]+):([0-9]+):([^:]+):([^:]+):([^:]+):([^:]+):([^:]+):([^:]+):([^:]+):(.+)$")
    return pool, tonumber(index), when, bidder, level, points, item, bidtaker, zone, link
end


function GLB.DecodeAwardDBDone(msg, sender)
    local pool, index = string.match(msg, "^awardeddone:([^:]+):([0-9]+)$")
    return pool, tonumber(index)
end


-- user interface API

function GLB.FullPoints(total_points)
    return math.max(GLB.MinimumBidPoints,
		    math.floor((total_points or 0) / 3 + 0.5))
end


function GLB.HalfPoints(total_points)
    return math.max(GLB.MinimumBidPoints,
		    math.floor((total_points or 0) / 6 + 0.5))
end


function GLB.BidForLevel(total_points, level)
    if level == "Full" then
	return GLB.FullPoints(total_points)
    elseif level == "Half" then
	return GLB.HalfPoints(total_points)
    elseif level == "5pt." then
	return 5
    end
end


function GLB.ConstrainLevel(bidder, level)
    local total_points = GLB.GetPoints(bidder)
    total_points = tonumber(total_points)
    if not total_points or total_points <= GLB.MinimumBidPoints then
	-- 20090226: must have at least 6 points to do a full or half bid
	level = "5pt."
    end
    return level
end


function GLB.EstimatePoints(bidder, level)
    local total_points = GLB.GetPoints(bidder)
    total_points = tonumber(total_points)
    if total_points then
	return GLB.BidForLevel(total_points, level)
    end
end


local recent_whispers_to = { }

function GLB.MaybeWhisper(recipient, message)
    -- whisper only if the bidder whispered first
    if recent_whispers_to[recipient] ~= message then
	SendChatMessage(GLB.WhisperPrefix..message, "WHISPER", nil, recipient)
	recent_whispers_to[recipient] = message
    end
end


function GLB.LogAward(when, bidder, level, points, item, link, bidtaker, zone)
    table.insert(GroupLootBidder_AwardDB[GroupLootBidder_PointPool], {
		     when = when,
		     bidder = bidder,
		     level = level,
		     points = points,
		     item = item,
		     link = link,
		     bidtaker = bidtaker,
		     zone = zone,
		 })
end


function GLB.AwardIndex(pool, bidder, level, points, item)
    local award_db = GroupLootBidder_AwardDB[pool]
    for i = #award_db, 1, -1 do
	local award = award_db[i]
	if (award.bidder == bidder
	    and award.level == level
	    and award.points == points
	    and award.item == item) then
	    return i
	end
    end
end


function GLB.LogRevokeAward(bidder, level, points, item)
    local i = GLB.AwardIndex(GroupLootBidder_PointPool, bidder, level, points, item)
    if i then
	local award_db = GroupLootBidder_AwardDB[GroupLootBidder_PointPool]
	table.remove(award_db, i)
    end
end


function GLB.IsOfficial(unit)
    -- if in a raid, player is a raid officer (assistant or raidleader)
    -- if in a party, player is the party leader
    -- otherwise, if in neither a raid or party, return true
    local in_raid = GetNumRaidMembers() > 0
    local in_party = GetNumPartyMembers() > 0
    if not unit then
	if in_raid then
	    return IsRaidOfficer()
	elseif in_party and GLB.EnableForParty then
	    return IsPartyLeader() or GLB.AllInPartyCanTakeBids
	else
	    -- for debugging
	    return true
	end
    else
	if in_raid then
	    return UnitIsRaidOfficer(unit)
	elseif in_party and GLB.EnableForParty then
	    return UnitIsPartyLeader(unit) or GLB.AllInPartyCanTakeBids
	else
	    -- for debugging
	    return true
	end
    end
end


local function order_by_when(a, b)
    return a.when < b.when
end

function GLB.Audit(bidder, pool)
    -- XXX non-nil pool is broken
    pool = pool or GroupLootBidder_PointPool
    local payouts = { }
    for raid_name, raid_info in pairs(GroupLootBidder_RaidDB) do
	-- hack
	for _, payout_info in pairs(raid_info.payouts) do
	    if raid_info.pool == pool and raid_info.members[bidder] then
		local when = payout_info.when
		if not when then
		    -- hack
		    local typical_raid_duration = 0400 -- 4hrs
		    when = tostring(tonumber(raid_name) + typical_raid_duration)
		end
		table.insert(payouts, { when = when,
					delta = payout_info.points,
					description = "payout by "..payout_info.payer })
	    end
	end
    end
    table.sort(payouts, order_by_when)
    local awards = { }
    for _, award in pairs(GroupLootBidder_AwardDB[GroupLootBidder_PointPool]) do
	if award.bidder == bidder then
	    local description = string.format("awarded %s for %s points (%d)",
					      award.link or award.item,
					      award.level,
					      award.points)
	    table.insert(awards, { when = award.when,
				   delta = -tonumber(award.points),
				   level = award.level,
				   description = description })
	end
    end
    table.sort(awards, order_by_when)
    local points = GLB.GetPoints(bidder)
    local ledger = { }
    while true do
	if (#payouts > 0
	    and (#awards == 0
		 or order_by_when(awards[-1], payouts[-1]) < 0)) then
	    -- either there's no awards, or the last remaining payout
	    -- is later than the last remaining award
	    local payout = table.remove(payouts)
	    points = points - payout.delta
	    payout.points = points
	    table.insert(ledger, 1, payout)
	elseif #awards > 0 then
	    -- possible adjustment and award
	    local award = table.remove(awards)
	    local level = GLB.capitalize(award.level)
	    local est = GLB.BidForLevel(points - award.delta, level)
	    local adjust = est + award.delta
	    if adjust ~= 0 then
		if level == "Full" then
		    adjust = adjust * 3
		    if award.level ~= level then
			award.leve = level
		    end
		elseif level == "Half" then
		    adjust = adjust * 6
		    if award.level ~= level then
			award.leve = level
		    end
		end
		points = points - adjust
		local description = string.format("inferred payout (%d)", adjust)
		table.insert(ledger, 1, { when = award.when,
					  delta = adjust,
					  points = points,
					  description = description })
	    end
	    points = points - award.delta
	    award.points = points
	    table.insert(ledger, 1, award)
	else
	    break
	end
    end
    if points ~= 0 then
	table.insert(ledger, 1, { when = ledger[1].when,
				  delta = 0,
				  points = points,
				  description = "initial value" })
    end
    return ledger
end


-- aliases

local old_GroupLootFrame_OnShow
local old_GroupLootFrame_OnHide
local old_GroupLootFrame_OnEvent
local old_GroupLootFrame_OpenNewFrame


-- widget handlers

-- XXX OnShow instead?
function GLB.BidderFrame_OnLoad(self)
    self:RegisterEvent("CHAT_MSG_ADDON")
    self:RegisterEvent("CHAT_MSG_WHISPER")
    self:RegisterEvent("PARTY_MEMBERS_CHANGED")
    self:RegisterEvent("RAID_INSTANCE_WELCOME")
    self:RegisterEvent("PLAYER_ENTERING_WORLD")
    -- hook on to intercept event that causes frame to hide,
    -- and to show/hide bid frames in tandem.
    if GroupLootFrame_OnShow ~= GLB.GroupLootFrame_OnShow then
	GLB.Debug("BidderFrame:Load()")
	old_GroupLootFrame_OnShow = GroupLootFrame_OnShow
	old_GroupLootFrame_OnHide = GroupLootFrame_OnHide
	old_GroupLootFrame_OnEvent = GroupLootFrame_OnEvent
	old_GroupLootFrame_OpenNewFrame = GroupLootFrame_OpenNewFrame
	GroupLootFrame_OnShow = GLB.GroupLootFrame_OnShow
	GroupLootFrame_OnHide = GLB.GroupLootFrame_OnHide
	GroupLootFrame_OnEvent = GLB.GroupLootFrame_OnEvent
	GroupLootFrame_OpenNewFrame = GLB.GroupLootFrame_OpenNewFrame
	-- 3.1.0 bugfix
	for i=1, NUM_GROUP_LOOT_FRAMES do
	    local f = getglobal("GroupLootFrame"..i)
	    f:SetScript("OnShow", GroupLootFrame_OnShow)
	    f:SetScript("OnEvent", GroupLootFrame_OnEvent)
	    f:SetScript("OnHide", GroupLootFrame_OnHide)
	end
    end
end


function GLB.SetRaid(raid_name)
    if raid_name ~= GroupLootBidder_CurrentRaidName then
	GroupLootBidder_RaidDB[raid_name] = GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName]
	GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName] = nil
	GroupLootBidder_CurrentRaidName = raid_name
    end
end


function GLB.NewRaid()
    -- WARNING: current_raid_name will be different per player... until it's announced
    GroupLootBidder_CurrentRaidName = GLB.GetTime()
    local raid_info = { zones = { }, members = { }, payouts = { } }
    GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName] = raid_info
end


function GLB.SetRaidPayout(raid_name, pool, payout_id, payer, earned, when)
    GLB.SetRaid(raid_name)
    GLB.SetPool(pool)
    local raid_info = GroupLootBidder_RaidDB[raid_name]
    if not next(raid_info.zones) then
	GLB.UpdateZone(GetZoneText())
    end
    for i = #raid_info.payouts, 1, -1 do
	if raid_info.payouts[i].payout_id == payout_id then
	    table.remove(raid_info.payouts, i)
	end
    end
    table.insert(raid_info.payouts, {
		     payout_id = payout_id,
		     points = earned,
		     pool = pool,
		     payer = payer,
		     when = when,
		 })
    GLB.ChatMessage(string.format("Payout of %d %s points per %s",
				  earned, pool, payer))
end


local purge_complete = false

function GLB.PurgeOldRaids()
    if not purge_complete then
	local purges_left = 3
	local purge_cutoff = GLB.GetTime(-GLB.PointlessRaidExpiration)
	local raids = GLB.table_keys(GroupLootBidder_RaidDB)
	table.sort(raids)
	for _, raid_name in ipairs(raids) do
	    if purges_left <= 0 or raid_name >= purge_cutoff then
		break
	    end
	    local raid_info = GroupLootBidder_RaidDB[raid_name]
	    if not raid_info.payouts or #raid_info.payouts == 0 then
		GLB.ChatMessage("Purging pointless raid "..raid_name)
		GroupLootBidder_RaidDB[raid_name] = nil
		purges_left = purges_left - 1
	    end
	end
	if purges_left > 0 then
	    purge_complete = true
	end
    end
end


function GLB.UpdateRaidMembers()
    if not GroupLootBidder_CurrentRaidName then
	-- will probably get updated after i send AnnounceVersion
	GLB.NewRaid()
	GLB.ChatMessage("Raid reset")
    end
    local added = { }
    -- XXX party?
    for i = 1, MAX_RAID_MEMBERS do
	local name = GetRaidRosterInfo(i)
	if not name then
	    break
	end
	local raid_info = GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName]
	if raid_info and not raid_info.members[name] then
	    raid_info.members[name] = GLB.GetTime()
	    table.insert(added, name)
	    GLB.Debug("UpdateRaidMembers adding "..name)
	end
    end
    if #added > 0 then
	table.sort(added)
	GLB.ChatMessage("Added "..table.concat(added, ", ").." to raid "..GroupLootBidder_CurrentRaidName)
    end
end


function GLB.UpdateZone(instance_name)
    if not GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName].zones[instance_name] then
	GLB.ChatMessage("Zone "..instance_name.." now part of raid "..GroupLootBidder_CurrentRaidName)
	GroupLootBidder_RaidDB[GroupLootBidder_CurrentRaidName].zones[instance_name] = GLB.GetTime()
    end
end


function GLB.BidderFrame_OnEvent(self, event, ...)
    -- background chores
    if event == "CHAT_MSG_ADDON" then
	local addon, msg, _, sender = ...
	if addon == GLB.AddonIdent then
	    GLB.PurgeOldRaids()
	    local version = GLB.DecodeVersion(msg, sender)
	    if version then
		local version_cmp = GLB.VersionCompare(version, GLB.MostRecentVersion)
		if version_cmp > 0 then
		    GLB.ChatMessage(string.format("Version %s is obsoleted by %s per %s; please update.",
						  GLB.MostRecentVersion,
						  version, sender))
		    GLB.MostRecentVersion = version
		elseif version_cmp < 0 then
		    if GLB.IsOfficial(sender) then
			GLB.ChatMessage(string.format("%s is using an obsolete version %s.", sender, version))
		    end
		else
		    GLB.Debug("You are running the same version as "..sender)
		end
		-- XXX may want to explicitly broadcast
		if GLB.IsOfficial() then
		    if GroupLootBidder_PointPool ~= "None" then
			GLB.AnnouncePool(GroupLootBidder_PointPool)
		    end
		    if GroupLootBidder_CurrentRaidName then
			GLB.AnnounceRaid(GroupLootBidder_CurrentRaidName)
		    end
		end
	    end
	    if GLB.IsOfficial() then
		local bidder = GLB.DecodePointsRequest(msg, sender)
		if bidder then
		    GLB.Debug("BidderFrame PointsRequest for "..bidder.." from "..sender)
		    local main, mtime, routing = GLB.GetAlt(bidder)
		    GLB.SendAltUpdate(bidder, main, mtime, routing)
		    local points, mtime, routing = GLB.GetPoints(bidder)
		    GLB.SendPointsUpdate(bidder, points, mtime, routing)
		end
	    end
	    -- only accept official communication
	    if GLB.IsOfficial(sender) then
		local item = GLB.DecodeTakingBids(msg, sender)
		if item then
		    -- XXX spammy
		    GLB.ChatMessage("Bidding of "..item.." now open by "..sender)
		end
		local pool = GLB.DecodePool(msg, sender)
		if pool and pool ~= GroupLootBidder_PointPool then
		    GLB.ChatMessage("Now using point pool "..pool.." per "..sender)
		    GLB.SetPool(pool)
		end
		local bidder, points, mtime, routing = GLB.DecodePointsUpdate(msg, sender)
		if bidder and sender ~= UnitName("player") then
		    GLB.Debug("BidderFrame PointsUpdate for "..bidder.." from "..sender)
		    GLB.SetPointsIfBetter(bidder, points, mtime, routing)
		end
		local when, bidder, level, points, item, link = GLB.DecodeAward(msg, sender)
		if bidder then
		    GLB.LogAward(when, bidder, level, points, item, link, sender, GetZoneText())
		end
		local bidder, level, points, item = GLB.DecodeRevokeAward(msg, sender)
		if bidder then
		    GLB.ChatMessage("Revoking award of "..item.." to "..bidder.." refunding "..points.." per "..sender)
		    GLB.LogRevokeAward(bidder, level, points, item)
		end
		local alt, main, mtime, routing = GLB.DecodeAltUpdate(msg, sender)
		if alt then
		    GLB.SetAltIfBetter(alt, main, mtime, routing)
		end
		local item = GLB.DecodeCloseBidding(msg, sender)
		if item then
		    -- XXX spammy
		    GLB.ChatMessage("Bidding of "..item.." has been closed by "..sender)
		end
		local raid_name = GLB.DecodeRaid(msg, sender)
		local twelve_hours_ago = GLB.GetTime(-0.5)
		if raid_name and twelve_hours_ago < raid_name and raid_name ~= GroupLootBidder_CurrentRaidName then
		    -- accept new raid names if:
		    --   my old raid name is >12hrs old
		    --   or i don't have a name yet
		    --   or i'm not an official
		    local named = GroupLootBidder_CurrentRaidName ~= nil
		    local earlier = named and raid_name < GroupLootBidder_CurrentRaidName
		    if (not named or earlier or not GLB.IsOfficial()) then
			-- XXX spammy
			GLB.ChatMessage("Setting raid name to "..raid_name.." per "..sender)
			GLB.SetRaid(raid_name)
		    end
		end
		local raid_name, pool, payout_id, payer, earned, when = GLB.DecodePayout(msg, sender)
		if raid_name then
		    GLB.SetRaidPayout(raid_name, pool, payout_id, payer, earned, when)
		end
	    end
	    local before = GLB.DecodeOldRaidNamesRequest(msg, sender)
	    if before then
		local next_biggest
		for raid_name in pairs(GroupLootBidder_RaidDB) do
		    if (next_biggest or "") < raid_name and raid_name < before then
			next_biggest = raid_name
		    end
		end
		GLB.SendOldRaidName(sender, next_biggest)
	    end
	    local raid_name = GLB.DecodeOldRaidName(msg, sender)
	    if raid_name then
		GLB.ChatMessage(string.format("previous raid on %s", raid_name))
	    end
	    local raid_names_done = GLB.DecodeOldRaidNamesDone(msg, sender)
	    if raid_names_done then
		GLB.ChatMessage("done with raid names")
	    end
	    local raid_name = GLB.DecodeRaidDBRequest(msg, sender)
	    if raid_name and GroupLootBidder_RaidDB[raid_name] then
		local members = GLB.table_keys(GroupLootBidder_RaidDB[raid_name]["members"])
		table.sort(members)
		members = table.concat(members, " ")
		local payouts_original = GroupLootBidder_RaidDB[raid_name]["payouts"] or { }
		local payouts = { }
		for i, payout in ipairs(payouts_original) do
		    if payout.pool then
			table.insert(payouts,
				     string.format("%s,%s,%s,%s,%s", payout.pool,
						   payout.payout_id, payout.payer,
						   payout.points, payout.when))
		    end
		end
		payouts = table.concat(payouts, " ")
		GLB.SendRaidDB(sender, raid_name, members, payouts)
	    end
	    local index, members, payouts = GLB.DecodeRaidDB(msg, sender)
	    if index then
		GLB.ChatMessage(string.format("raid on %s had %s", index, members))
		if payouts ~= "" then
		    GLB.ChatMessage(string.format("raid on %s paid %s", index, payouts))
		end
	    end
	    local pool, index = GLB.DecodeAwardDBRequest(msg, sender)
	    if pool then
		local ap = GroupLootBidder_AwardDB[pool]
		if ap and ap[index] then
		    local award = ap[index]
		    GLB.SendAwardDB(sender, pool, index, award.when,
				    award.bidder, award.level, award.points,
				    award.item, award.bidtaker, award.zone, award.link)
		else
		    GLB.SendAwardDB(sender, pool, index)
		end
	    end
	    local pool, index, when, bidder, level, points, item, bidtaker, zone, link = GLB.DecodeAwardDB(msg, sender)
	    if pool then
		GLB.ChatMessage(string.format("award pool %s[%s] on %s to %s %s bid for %s points for %s by %s in %s",
					      pool, index, when, bidder, level, points, link or item, bidtaker, zone))
		if not GLB.AwardIndex(pool, bidder, level, points, item) then
		    GLB.LogAward(when, bidder, level, points, item, link, bidtaker, zone)
		    GLB.ChatMessage("added to database")
		end
	    end
	    local pool, index = GLB.DecodeAwardDBDone(msg, sender)
	    if pool then
		GLB.ChatMessage("done with awards")
	    end
	end
    elseif event == "CHAT_MSG_WHISPER" then
	if GLB.IsOfficial() then
	    local msg, sender = ...
	    if UnitInRaid(sender) or UnitInParty(sender) or sender == UnitName("player") then
		local function reply_with_points()
		    local points = GLB.GetPoints(sender) or "no"
		    SendChatMessage(string.format("%sYou have %s points.",
						  GLB.WhisperPrefix, points),
				    "WHISPER", nil, sender)
		end
		local function reply_with_who()
		    local replies = 0
		    -- XXX need to uses a disjunct data structure
		    for _, item in pairs(GLB.items) do
			if item.bid_taker and item.link then
			    SendChatMessage(string.format("%s%s taking %s",
							  GLB.WhisperPrefix,
							  item.bid_taker,
							  item.link),
					    "WHISPER", nil, sender)
			    replies = replies + 1
			end
		    end
		    if replies == 0 then
			SendChatMessage(string.format("%sNobody is taking bids yet.",
						      GLB.WhisperPrefix),
					"WHISPER", nil, sender)
		    end
		end
		local function reply_with_audit()
		    for _, row in pairs(GLB.Audit(sender)) do
			SendChatMessage(string.format("%s %s  %d -> %d  %s",
						      GLB.WhisperPrefix,
						      row.when,
						      row.points,
						      row.points + row.delta,
						      row.description),
				    "WHISPER", nil, sender)
		    end
		end
		local whisper_patterns = {
		    ["^[dD][kK][pP]$"] = reply_with_points,
		    ["^[pP][oO][iI][nN][tT][sS]$"] = reply_with_points,
		    ["^[wW][hH][oO][mM]?$"] = reply_with_who,
		    ["^[aA][uU][dD][iI][tT]$"] = reply_with_audit,
		}
		for pattern, fn in pairs(whisper_patterns) do
		    if string.match(msg, pattern) then
			fn()
			break
		    end
		end
	    end
	end
    elseif event == "PARTY_MEMBERS_CHANGED" then
	local in_raid = GetNumRaidMembers() > 0
	local in_party = GetNumPartyMembers() > 0
	if in_raid or (in_party and GLB.EnableForParty) then
	    GLB.UpdateRaidMembers()
	else
	    if GroupLootBidder_CurrentRaidName then
		-- player left the raid
		GroupLootBidder_CurrentRaidName = nil
	    end
	end
    elseif event == "RAID_INSTANCE_WELCOME" then
	-- triggers on boss kill
	-- what about glass grinding nights?
	local instance_name = ...
	GLB.UpdateRaidMembers()
	GLB.UpdateZone(instance_name)
	-- XXX is this often enough?
	GLB.AnnounceVersion()
    elseif event == "PLAYER_ENTERING_WORLD" then
	RegisterAddonMessagePrefix(GLB.AddonIdent)
	GLB.UpdateRaidMembers()
	GLB.AnnounceVersion()
    end
end


function GLB.GroupLootFrame_OnShow(self)
    old_GroupLootFrame_OnShow(self)
    GLB.Debug("Frame:Show() [post-hook] for #"..self:GetID())
    -- show bids frame if i'm an officer
    local bid_frame = getglobal(self:GetName().."BidFrame")
    local bids_frame = getglobal(self:GetName().."BidsFrame")
    if GLB.IsOfficial() then
	bids_frame:Show()
    end
    -- wait for bid-taker to click "take bids" before showing the bid buttons
    -- XXX redundant?
    bid_frame:Hide()
end


function GLB.GroupLootFrame_OnHide(self)
    -- reset everything that could have changed
    GLB.Debug("Frame:Hide() [pre-hook] for #"..self:GetID())
    getglobal(self:GetName().."BidFrame"):Hide()
    getglobal(self:GetName().."BidsFrame"):Hide()
    getglobal(self:GetName().."PassButton"):Show()
    getglobal(self:GetName().."Corner"):Show()
    getglobal(self:GetName().."RollButton"):Show()
    getglobal(self:GetName().."GreedButton"):Show()
    getglobal(self:GetName().."DisenchantButton"):Show()
    old_GroupLootFrame_OnHide(self)
    if (#GLB.PendingLoots > 0) then
	GLB.Debug("Frame:Hide() pending loots for #"..self:GetID())
	local new_loot = table.remove(GLB.PendingLoots, 1)
	old_GroupLootFrame_OpenNewFrame(new_loot.id, new_loot.rollTime)
    end
end


function GLB.GroupLootFrame_OnEvent(self, event, ...)
    local bids_frame = getglobal(self:GetName().."BidsFrame")
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    local bids_frame_shown = bids_frame:IsShown()
    if event == "CANCEL_LOOT_ROLL" and bids_frame_shown then
	local rollID = ...
	if rollID == self.rollID then
	    -- intercept the timed auto-hide of the roll frame
	    GLB.Debug("Frame CANCEL_LOOT_ROLL for #"..bids_frame:GetParent():GetID())
	    getglobal(self:GetName().."PassButton"):Hide()
	    getglobal(self:GetName().."Corner"):Hide()
	    getglobal(self:GetName().."RollButton"):Hide()
	    getglobal(self:GetName().."GreedButton"):Hide()
	    getglobal(self:GetName().."DisenchantButton"):Hide()
	    return
	end
    end
    old_GroupLootFrame_OnEvent(self, event, ...)
end


function GLB.HideLootFrame(loot_frame)
    old_GroupLootFrame_OnEvent(loot_frame, "CANCEL_LOOT_ROLL", loot_frame.rollID)
end


function GLB.GroupLootFrame_OpenNewFrame(id, rollTime)
    for i = 1, NUM_GROUP_LOOT_FRAMES do
	local frame = _G["GroupLootFrame".. i];
	if (not frame:IsShown()) then
	    return old_GroupLootFrame_OpenNewFrame(id, rollTime)
	end
    end
    -- possibly passed but not hidden
    table.insert(GLB.PendingLoots, { id = id, rollTime = rollTime })
end
