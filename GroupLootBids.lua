-- Copyright (c) 2009-2011, r. brian harrison.  All rights reserved.


-- aliases

local GLB = GroupLootBidder

GLB.MAX_BIDS = 40
GLB.MAX_VISIBLE_BIDS = 6


-- utilities


local function NN(s)
    -- Not nil
    return s or "nil"
end


local function format_to_pattern(s)
    return "^"..string.gsub(s, "%%s", "(.+)").."$"
end


local function count_frames_with_item(item_name)
    -- warning: the count changes as roll frames are closed and new ones take their place
    local count = 0
    for bids_frame in GLB.BidsFrames(item_name) do
	count = count + 1
    end
    return count
end


function GLB.Officials()
    local i = 0
    return function()
	       while i < MAX_RAID_MEMBERS do
		   i = i + 1
		   local name = GetRaidRosterInfo(i)
		   if GLB.IsOfficial(name) then
		       return name
		   end
	       end
	   end
end


function GLB.PassedOfficials(item)
    local iter = GLB.Officials()
    local count = count_frames_with_item(item.name)
    return function()
	       for official in iter do
		   -- slight bug: if other official has more exposed copies of same item
		   if (item.passed[official] or 0) >= count then
		       return official
		   end
	       end
	   end
end


function GLB.BidTakingBlockingOfficial(item)
    -- only returns the first
    for official in GLB.Officials() do
	if not item.go_aheads[official] then
	    return official
	end
    end
end


function GLB.BidsFrames(item_name)
    if item_name then
	local iter = GLB.BidsFrames()
	return function()
		   while true do
		       local frame = iter()
		       if not frame or frame.item and frame.item.name == item_name then
			   return frame
		       end
		   end
	       end
    else
	local i = 0
	return function()
		   if i < NUM_GROUP_LOOT_FRAMES then
		       i = i + 1
		       return getglobal("GroupLootFrame"..i.."BidsFrame")
		   end
	       end
    end
end


--/script GroupLootBidder.UpdateFrames("Mutton Chop")

function GLB.UpdateFrames(item_name)
    for bids_frame in GLB.BidsFrames(item_name) do
	GLB.BidsFrame_Update(bids_frame)
	local awaiting_frame = getglobal(bids_frame:GetName().."AwaitingGoAheadFrame")
	if awaiting_frame:IsShown() then
	    GLB.AwaitingGoAheadFrame_Update(awaiting_frame)
	end
	local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
	if manage_bids_frame:IsShown() then
	    GLB.ManageBidsFrame_Update(manage_bids_frame)
	end
    end
end


function GLB.GetItem(item_name)
    local item = GLB.items[item_name]
    if not item then
	item = {
	    name = item_name,
	    link = nil,
	    bid_taker = nil,
	    awaiting = false,
	    awarded = false,
	    done = false,
	    manual = false,
	    bids = { },
	    whispers = { },
	    go_aheads = { },
	    passed = { },
	    countdown = nil,
	    next_countdown = 1,
	}
	GLB.items[item_name] = item
    end
    return item
end


function GLB.ClearItem(item_name)
    GLB.items[item_name] = nil
end


function GLB.ResetItems()
    GLB.items = { }
end


function GLB.GetActiveItem()
    for name, item in pairs(GLB.items) do
	if item.bid_taker == UnitName("player") and not item.awaiting and not item.awarded then
	    return item
	end
    end
end


function GLB.GetBidByName(item, bidder)
    local bids = item.bids
    for i = 1, #bids do
	if bids[i].bidder == bidder then
	    return i, bids[i]
	end
    end
end


local level_order = { ["Tell"] = 1, ["5pt."] = 2, ["Half"] = 3, ["Full"] = 4 }

local function level_compare(level1, level2)
    level1 = level_order[level1] or 0
    level2 = level_order[level2] or 0
    return level1 - level2
end


local function points_compare(points1, points2)
    -- positive if points1 > points2
    points1 = tonumber(points1) or 0
    points2 = tonumber(points2) or 0
    return points1 - points2
end


function GLB.InsertBid(item, new_bid, total_points)
    local bids = item.bids
    local vacancy = #bids + 1
    for i = #bids, 1, -1 do
	local bid = bids[i]
	local level_comparison = level_compare(new_bid.level, bid.level)
	local points_comparison = points_compare(total_points, bid.total_points)
	if (level_comparison < 0
	    or (level_comparison == 0 and points_comparison < 0)) then
	    break
	end
	vacancy = i
    end
    table.insert(item.bids, vacancy, new_bid)
end


-- XXX is table recyling really still necessary?
local recycled_bids = { }

function GLB.SetBid(item, level, bid_points, bidder, total_points)
    local new_bid = table.remove(recycled_bids) or { }
    new_bid.level = level
    new_bid.points = bid_points
    new_bid.bidder = bidder
    new_bid.won = false
    new_bid.whispered = false
    new_bid.total_points = total_points
    GLB.InsertBid(item, new_bid, total_points)
end


-- XXX RecalculateBid(item, bid)?
function GLB.RecalculateBid(item, bidder)
    local i, bid = GLB.GetBidByName(item, bidder)
    if bid then
	local new_total_points = GLB.GetPoints(bidder)
	local new_level = GLB.ConstrainLevel(bidder, bid.level)
	local new_points = GLB.EstimatePoints(bidder, new_level, new_total_points)
	-- DeleteBid(item, bidder) wipes out bid attributes
	table.remove(item.bids, i)
	bid.total_points = new_total_points
	bid.level = new_level
	bid.points = new_points
	GLB.InsertBid(item, bid, new_total_points)
    end
end


-- XXX DeleteBid(item, bid)?
function GLB.DeleteBid(item, bidder)
    local i, bid = GLB.GetBidByName(item, bidder)
    if i then
	local bids = item.bids
	table.wipe(bids[i])
	table.insert(recycled_bids, bids[i])
	table.remove(bids, i)
	return bid
    end
end


function GLB.WipeBids(item)
    local bids = item.bids
    for k in ipairs(bids) do
	table.wipe(bids[k])
	table.insert(recycled_bids, bids[k])
	bids[k] = nil
    end
end


function GLB.BidsFrame_WipeBids(bids_frame)
    GLB.WipeBids(bids_frame.item)
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    GLB.ManageBidsFrame_Update(manage_bids_frame)
end


function GLB.BidsFrame_DeleteBid(bids_frame, bidder)
    GLB.DeleteBid(bids_frame.item, bidder)
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    GLB.ManageBidsFrame_Update(manage_bids_frame)
end


function GLB.BidsFrame_SetBid(bids_frame, level, bid_points, bidder, total_points)
    GLB.BidsFrame_DeleteBid(bids_frame, bidder)
    GLB.SetBid(bids_frame.item, level, bid_points, bidder, total_points)
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    GLB.ManageBidsFrame_Update(manage_bids_frame)
end


function GLB.BidsFrame_RecalculateBid(bids_frame, bidder)
    GLB.RecalculateBid(bids_frame.item, bidder)
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    GLB.ManageBidsFrame_Update(manage_bids_frame)
end


local winners, ties, uncertain = { }, { }, { }

function GLB.WinnersTiesAndUncertain(bids, count)
    local losers = 0
    table.wipe(winners)
    table.wipe(ties)
    table.wipe(uncertain)
    local previous_bid = bids[1]
    if not previous_bid then
	-- no bids
	return winners, ties, uncertain
    end
    if tonumber(previous_bid.points) then
	ties[1] = previous_bid
    else
	uncertain[1] = previous_bid
    end
    for i = 2, GLB.MAX_BIDS do
	local bid = bids[i]
	if bid and previous_bid.level == bid.level and not tonumber(bid.points) then
	    -- same bidding tier, but can't calculate their points
	    table.insert(uncertain, bid)
	elseif bid and previous_bid.level == bid.level and previous_bid.total_points == bid.total_points then
	    -- exactly tied
	    table.insert(ties, bid)
	elseif i <= count + 1 then
	    -- still looking for the requested number of winners
	    if bid then
		if (previous_bid.level == bid.level
		    and previous_bid.points <= bid.points
		    and previous_bid.total_points <= bid.total_points) then
		    GLB.ChatWarning(string.format("WinnersTiesAndUncertain diagnostics: count %s vs. %s, level %s vs. %s, points %s vs. %s, total_points %s vs. %s",
					      (i or "nil"),
					      (count or "nil"),
					      (previous_bid.level or "nil"),
					      (bid.level or "nil"),
					      (previous_bid.points or "nil"),
					      (bid.points or "nil"),
					      (previous_bid.total_points or "nil"),
					      (bid.total_points or "nil")))
		    --assert(previous_bid.level ~= bid.level or previous_bid.points > bid.points or previous_bid.total_points > bid.total_points)
		end
	    end
	    -- all the ties will win
	    for k, v in ipairs(ties) do
		table.insert(winners, v)
		ties[k] = nil
	    end
	    -- the uncertains will all win
	    if not bid or previous_bid.level ~= bid.level then
		for k, v in ipairs(uncertain) do
		    table.insert(winners, v)
		    uncertain[k] = nil
		end
	    end
	    if bid and i == count + 1 and previous_bid.level ~= bid.level then
		-- we've concurrently accumulated enough winners and the next bid is of a different level
		break
	    end
	    ties[1] = bid
	elseif bid and previous_bid.level ~= bid.level then
	    -- done collecting uncertains at the final level
	    break
	elseif bid then
	    -- past the requested number of winners and not tied but still at the same level as the (lowest) winner
	    losers = losers + 1
	    if (i <= count
		or previous_bid.level ~= bid.level
		or previous_bid.points == bid.points
		or not tonumber(bid.points)) then
		GLB.ChatWarning(string.format("WinnersTiesAndUncertain diagnostics: count %s vs. %s, level %s vs. %s, points %s vs. %s",
					      (i or "nil"),
					      (count or "nil"),
					      (previous_bid.level or "nil"),
					      (bid.level or "nil"),
					      (previous_bid.points or "nil"),
					      (bid.points or "nil")))
	    end
	    --assert(i > count and previous_bid.level == bid.level and previous_bid.points ~= bid.points and tonumber(bid.points))
	end
	if not bid then
	    break
	end
	previous_bid = bid
	assert(#winners + #ties + #uncertain + losers == i)
    end
    if #winners == count and count < GLB.MAX_BIDS then
	for i in ipairs(ties) do
	    ties[i] = nil
	end
    else
	-- could be tied with the GLB.MAX_BIDS+1 bid
    end
    if uncertain[1] then
	-- hack to demote winners into tied if they're the same level as uncertains.  this miscategorization happened because there was a loser (same level, fewer points) between.
	for i, v in ipairs(winners) do
	    if v.level == uncertain[1].level then
		table.insert(ties, v)
		winners[i] = nil
	    end
	end
    end
    return winners, ties, uncertain
end


-- widget handlers


local function bop_frame(self)
    self:SetBackdrop({ bgFile = "Interface\\DialogFrame\\UI-DialogBox-Gold-Background", edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Gold-Border", tile = true, tileSize = 32, edgeSize = 32, insets = { left = 11, right = 12, top = 12, bottom = 11 } })
    getglobal(self:GetName().."Corner"):SetTexture("Interface\\DialogFrame\\UI-DialogBox-Gold-Corner")
end

local function boe_frame(self)
    self:SetBackdrop({ bgFile = "Interface\\DialogFrame\\UI-DialogBox-Background", edgeFile = "Interface\\DialogFrame\\UI-DialogBox-Border", tile = true, tileSize = 32, edgeSize = 32, insets = { left = 11, right = 12, top = 12, bottom = 11 } })
    getglobal(self:GetName().."Corner"):SetTexture("Interface\\DialogFrame\\UI-DialogBox-Corner")
end


function GLB.BidsFrame_OnShow(self)
    GLB.Debug("BidsFrame:Show() for #"..self:GetParent():GetID())
    local rollID = self:GetParent().rollID
    local _, item_name, _, _, bind_on_pickup = GetLootRollItemInfo(rollID)
    local link = GetLootRollItemLink(rollID)
    if bind_on_pickup then
	bop_frame(self)
    else
	boe_frame(self)
    end
    local item = GLB.GetItem(item_name)
    assert(item ~= nil)
    assert(item.name ~= nil)
    -- XXX is link truly unique?
    item.link = link
    -- need to keep a copy of item info since this frame may live beyond the validity of GetLootRollItemInfo()
    self.item = item
    -- hide my TakeBidsFrame
    -- hide or show BidTakerFrame
    -- hide or show ManageBidsFrame (depending upon duplicate item in progress)
    -- special case if there's multiple copies of this item
    for bids_frame in GLB.BidsFrames(item.name) do
	if bids_frame.item ~= nil then
	    GLB.BidsFrame_Update(bids_frame)
	end
    end
end


function GLB.BidsFrame_OnHide(self)
    -- XXX warning: 0.14 and before does not send the BiddingDone packet
    -- XXX warning: if nobody takes bids on this item, it never purges
    if self.item.done then
	local all_done = true
	for bids_frame in GLB.BidsFrames(self.item) do
	    if bids_frame:IsShown() then
		all_done = false
	    end
	end
	if all_done then
	    GLB.ClearItem(self.item.name)
	end
	self.item = nil
    end
    getglobal(self:GetName().."TakeBidsFrame"):Hide()
    getglobal(self:GetName().."AwaitingGoAheadFrame"):Hide()
    getglobal(self:GetName().."BidsTakerFrame"):Hide()
    getglobal(self:GetName().."ManageBidsFrame"):Hide()
end


function GLB.BidsFrame_Update(self)
    local function hide(suffix)
	getglobal(self:GetName()..suffix):Hide()
    end
    local function show(suffix)
	getglobal(self:GetName()..suffix):Show()
    end
    local function disable(suffix)
	getglobal(self:GetName()..suffix):Disable()
    end
    local function enable(suffix)
	getglobal(self:GetName()..suffix):Enable()
    end
    -- discern how to render based on the model
    local item = self.item
    -- BidsFrame
    if not item or not item.name then
	self:Hide()
	return
    end
    local me = UnitName("player")
    local mine = item.bid_taker and item.bid_taker == me
    local someone_elses = item.bid_taker and item.bid_taker ~= me
    local passed = (item.passed[me] or 0) >= count_frames_with_item(item.name)
    local taking_bids_on_something_else = false
    for bids_frame in GLB.BidsFrames() do
	local that_item = bids_frame.item
	if (that_item
	    and that_item.bid_taker == me
	    and that_item.name ~= item.name
	    and not that_item.done) then --???
	    taking_bids_on_something_else = true
	    break
	end
    end
    GLB.Debug(string.format("BidsFrame_Update for %s me=%s item=%s mine=%s elses=%s taking_else=%s",
			    NN(self:GetName()), NN(me), NN(item.name),
			    NN(mine and "true"), NN(someone_elses and "true"),
			    NN(taking_bids_on_something_else and "true")))
    -- TakeBidsFrame
    if not taking_bids_on_something_else and not someone_elses and not mine then
	show("TakeBidsFrame")
    else
	hide("TakeBidsFrame")
    end
    -- AwaitingGoAheadFrame
    if mine and item.awaiting then
	show("AwaitingGoAheadFrame")
    else
	hide("AwaitingGoAheadFrame")
    end
    -- BidsTakerFrame
    --if someone_elses then
    if someone_elses and not passed then
	local bid_taker = getglobal(self:GetName().."BidsTakerFrameBidTaker")
	if item.done or item.awarded then
	    bid_taker:SetText(string.format("%s took bids", item.bid_taker))
	else
	    bid_taker:SetText(string.format("%s is taking bids", item.bid_taker))
	end
	show("BidsTakerFrame")
    else
	hide("BidsTakerFrame")
    end
    -- ManageBidsFrame
    --if mine and not item.awaiting and not item.awarded then
    if ((mine or (someone_elses and passed))
	and not item.awaiting) then
	if someone_elses then
	    hide("ManageBidsFrameDisenchantButton")
	    hide("ManageBidsFrameAwardButton")
	else
	    if item.awarded then
		disable("ManageBidsFrameDisenchantButton")
		disable("ManageBidsFrameAwardButton")
	    else
		enable("ManageBidsFrameDisenchantButton")
		enable("ManageBidsFrameAwardButton")
	    end
	    if #item.bids > 0 then
		hide("ManageBidsFrameDisenchantButton")
		show("ManageBidsFrameAwardButton")
	    else
		show("ManageBidsFrameDisenchantButton")
		hide("ManageBidsFrameAwardButton")
	    end
	end
	if item.countdown then
	    show("ManageBidsFrameTimer")
	    hide("ManageBidsFrameWarnButton")
	    disable("ManageBidsFrameDisenchantButton")
	    disable("ManageBidsFrameAwardButton")
	else
	    hide("ManageBidsFrameTimer")
	    show("ManageBidsFrameWarnButton")
	    enable("ManageBidsFrameDisenchantButton")
	    enable("ManageBidsFrameAwardButton")
	end
	show("ManageBidsFrame")
    else
	hide("ManageBidsFrame")
    end
end


local function edit_points_commit(self)
    local points = tonumber(GroupLootBidderEditPointsFrameEditBox:GetText())
    local bidder = self:GetParent().bidder
    local bids_frame = self:GetParent().bids_frame
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    -- always overrides, so don't check mtime/routing
    GLB.SetPoints(bidder, points)
    local points, mtime, routing = GLB.GetPoints(bidder)
    GLB.SendPointsUpdate(bidder, points, mtime, routing)
    local msg = "You now have "..points.." points."
    GLB.MaybeWhisper(bidder, msg)
end


function GLB.EditPointsFrameEditBox_OnEnterPressed(self)
    edit_points_commit(self)
    self:GetParent():Hide()
    ChatFrameEditBox:SetFocus()
end


function GLB.EditPointsFrameEditBox_OnEscapePressed(self)
    self:GetParent():Hide()
    ChatFrameEditBox:SetFocus()
end


function GLB.EditPointsFrameLeftButton_OnClick(self)
    local edit_box = GroupLootBidderEditPointsFrameEditBox
    edit_box:SetText(tonumber(edit_box:GetText()) - 1)
end


function GLB.EditPointsFrameRightButton_OnClick(self)
    local edit_box = GroupLootBidderEditPointsFrameEditBox
    edit_box:SetText(tonumber(edit_box:GetText()) + 1)
end


function GLB.EditPointsFrameOkayButton_OnClick(self)
    edit_points_commit(self)
    self:GetParent():Hide()
end


function GLB.EditPointsFrameCancelButton_OnClick(self)
    self:GetParent():Hide()
end


function GLB.BidRowButton_OnClick(self, button, down)
    local bids_frame = self:GetParent():GetParent()
    local item_name = bids_frame.item.name
    local bid_level = getglobal(self:GetName().."Level"):GetText()
    local bidder = getglobal(self:GetName().."Bidder"):GetText()
    if bid_level and bidder then
	EasyMenu({ {
		       text = bidder..":",
		       isTitle = true,
		   }, {
		       text = "Full",
		       value = "Full",
		       checked = (bid_level == "Full"),
		       func = function()
				  GLB.PlaceBid(bidder, "Full", item_name, nil)
			      end,
		   }, {
		       text = "Half",
		       value = "Half",
		       checked = (bid_level == "Half"),
		       func = function()
				  GLB.PlaceBid(bidder, "Half", item_name, nil)
			      end,
		   }, {
		       text = "5pt.",
		       value = "5pt.",
		       checked = (bid_level == "5pt."),
		       func = function()
				  GLB.PlaceBid(bidder, "5pt.", item_name, nil)
			      end,
		   }, {
		       text = "Remove",
		       func = function()
				  GLB.AnnounceCancelBid(bidder, item_name)
			      end,
		   }, {
		       text = "Edit Points",
		       func = function()
				  local points = GLB.GetPoints(bidder) or 0
				  GroupLootBidderEditPointsFrame.bidder = bidder
				  GroupLootBidderEditPointsFrame.bids_frame = bids_frame
				  GroupLootBidderEditPointsFrame:SetPoint("TOPLEFT", self, "BOTTOMLEFT", 0, 4)
				  GroupLootBidderEditPointsFrameEditBox:SetText(points)
				  GroupLootBidderEditPointsFrame:Show()
			      end,
		   }, {
		       text = "Close",
	       } }, GroupLootBidRowDropDownMenu, self:GetName(), 0, 0, "MENU")
    end
end


function GLB.BidRowButton_OnEnter(self, motion)
    local manage_bids = self:GetParent()
    local bids_frame = manage_bids:GetParent()
    local bidder = getglobal(self:GetName().."Bidder"):GetText()
    local points = GLB.GetPoints(bidder)
    if not points then
	points = "unknown"
    end
    if bidder then
	GameTooltip:SetOwner(self, "ANCHOR_BOTTOMRIGHT")
	local text = string.format("%s has %s points\n", bidder, points)
	local whispers = bids_frame.item.whispers[bidder]
	if whispers then
	    text = text.."\nRecent whispers:"..whispers
	end
	GameTooltip:SetText(text)
    end
end


function GLB.BidRowButton_OnLeave(self)
    GameTooltip:Hide()
end


local function bids_frame_close(bids_frame)
    local loot_frame = bids_frame:GetParent()
    if getglobal(loot_frame:GetName().."PassButton"):IsShown() then
	-- timer hasn't expired and the bid taker could roll for it
	bids_frame:Hide()
    else
	-- the timer has either expired or the bid taker clicked pass already
	-- we re using a visible roll frame's rollID possibly after the timer, just like Blizzard's OnEvent
	GLB.HideLootFrame(loot_frame)
    end
end


StaticPopupDialogs["GROUP_LOOT_BIDS_CLOSE_BEFORE_AWARD"] = {
    text = "Are you sure you want to stop taking bids before awarding?  This may screw up the points database and cause unfair distribution of loot.",
    button1 = OKAY,
    button2 = CANCEL,
    OnAccept = function(self, bids_frame)
		   bids_frame_close(bids_frame)
	       end,
    timeout = 0,
    exclusive = 1,
    whileDead = 1,
    hideOnEscape = 1,
}

function GLB.BidsCloseButton_OnClick(self)
    local bids_frame = self:GetParent()
    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
    local item = bids_frame.item
    if item.awarded or item.bid_taker ~= UnitName("player") then
	bids_frame_close(bids_frame)
    else
	local dialog = StaticPopup_Show("GROUP_LOOT_BIDS_CLOSE_BEFORE_AWARD")
	if dialog then
	    dialog.data = bids_frame
	end
    end
end


function GLB.BidsCloseButton_OnEnter(self, motion)
    local text = "Close bid tracker."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.BidsCloseButton_OnLeave(self)
    GameTooltip:Hide()
end


StaticPopupDialogs["GROUP_LOOT_BIDS_OLD_VERSION"] = {
    text = "Are you sure you want to take bids with an obsolete %s?",
    button1 = OKAY,
    button2 = CANCEL,
    OnAccept = function(self, bids_frame)
		   GLB.AnnounceAboutToTakeBids(bids_frame.item.name)
	       end,
    timeout = 0,
    exclusive = 1,
    whileDead = 1,
    hideOnEscape = 1,
}

function GLB.BidsTakeBidsButton_OnClick(self, button, down)
    local bids_frame = self:GetParent():GetParent()
    if GLB.MostRecentVersion ~= GLB.VERSION then
	local dialog = StaticPopup_Show("GROUP_LOOT_BIDS_OLD_VERSION", GLB.AddonIdent)
	if dialog then
	    dialog.data = bids_frame
	end
    else
	GLB.AnnounceAboutToTakeBids(bids_frame.item.name)
    end
end


function GLB.BidsTakeBidsButton_OnEnter(self, motion)
    local text = "Receive bids for this item."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.BidsTakeBidsButton_OnLeave(self)
    GameTooltip:Hide()
end


--/script GroupLootBidder.AwaitingGoAheadFrame_Update(GroupLootFrame1BidsFrameAwaitingGoAheadFrame)

function GLB.AwaitingGoAheadFrame_Update(self)
    local bids_frame = self:GetParent()
    local item = bids_frame.item
    local waiting_on = getglobal(self:GetName().."WaitingOn")
    local blocker = GLB.BidTakingBlockingOfficial(item) or "Nobody"
    waiting_on:SetText(string.format("Waiting on %s", blocker))
end


function GLB.AwaitingGoAheadOverrideButton_OnClick(self, button, down)
    local bids_frame = self:GetParent():GetParent()
    GLB.AnnounceTakingBids(bids_frame.item.name)
end


function GLB.AwaitingGoAheadOverrideButton_OnEnter(self, motion)
    -- XXX should display who hasn't ack'd
    local text = "Not all of the officials have acknowledged your intent to take bids yet.\nClick to override this safety mechanism."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.AwaitingGoAheadOverrideButton_OnLeave(self)
    GameTooltip:Hide()
end


function GLB.BidsWarnButton_OnClick(self, button, down)
    local bids_frame = self:GetParent():GetParent()
    local item = bids_frame.item
    GLB.Alert(string.format("%s is taking bids for %s and will be awarded soon",
			    item.bid_taker, item.link))
end


function GLB.BidsWarnButton_OnEnter(self, motion)
    local text = "Announce that the end of bidding is approaching,\nand that final bids should be placed now."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.BidsWarnButton_OnLeave(self)
    GameTooltip:Hide()
end


function GLB.BidsDisenchantButton_OnClick(self, button, down)
    local manage_bids_frame = self:GetParent()
    local bids_frame = manage_bids_frame:GetParent()
    local item = bids_frame.item
    GLB.AnnounceCloseBidding(item.name)
    GLB.AnnounceDisenchant(item.name)
end


function GLB.BidsDisenchantButton_OnEnter(self, motion)
    local text = "Do not award the item but instead request it to be disenchanted."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.BidsDisenchantButton_OnLeave(self)
    GameTooltip:Hide()
end


local winning_bidders = { }

function GLB.BidsAwardButton_OnClick(self, button, down)
    local item = self:GetParent():GetParent().item
    if item.awarded then
	GLB.ChatMessage(item.link.." has already been awarded.")
	return
    end
    local identical_items = count_frames_with_item(item.name)
    local winners, ties, uncertain = GLB.WinnersTiesAndUncertain(item.bids, identical_items)
    table.wipe(winning_bidders)
    for _, bid in ipairs(winners) do
	if not tonumber(bid.points) then
	    GLB.ChatMessage("No bid points for winner "..(bid.bidder or "nil"))
	    return
	end
    end
    for _, bid in ipairs(ties) do
	if not tonumber(bid.points) then
	    GLB.ChatMessage("No bid points for tied "..(bid.bidder or "nil"))
	    return
	end
    end
    for _, bid in ipairs(uncertain) do
	GLB.ChatMessage("No bid points for possible winner "..(bid.bidder or "nil"))
	return
    end
    -- randomly pick among the tied
    while #ties > 0 and #winners < identical_items do
	table.insert(winners, table.remove(ties, math.random(#ties)))
    end
    for _, bid in ipairs(winners) do
	table.insert(winning_bidders, bid.bidder)
    end
    table.sort(winning_bidders)
    local winners_string = table.concat(winning_bidders, ", ")
    if #winners > 0 then
	GLB.AnnounceCloseBidding(item.name)
	for _, bid in ipairs(winners) do
	    GLB.AnnounceAward(bid.bidder, bid.level, bid.points, item.name, item.link)
	end
	item.awarded = #winners
    else
	GLB.ChatMessage("Winners not determined yet.")
    end
end


function GLB.BidsAwardButton_OnEnter(self, motion)
    local text = "Pick the top bid(s) and announce them winner(s).\nDocks them the appropriate number of points."
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(text)
end


function GLB.BidsAwardButton_OnLeave(self)
    GameTooltip:Hide()
end


local BID_WON_COLOR = { 0, 1, 0, 1 }
local BID_NORMAL_COLOR = { 1, 1, 1, 1 }
local BID_UNCERTAIN_COLOR = { 1, 0, 0, 1 }
local BID_TELL_COLOR = { 1, 0.3, 0.3, 1 }

function GLB.BidsFrameManageBidsScrollBar_Update(self)
    local manage_bids_frame = self:GetParent()
    if manage_bids_frame:GetParent().item == nil then
	-- XXX hack
	return
    end
    local bids = manage_bids_frame:GetParent().item.bids
    FauxScrollFrame_Update(self, #bids, GLB.MAX_VISIBLE_BIDS, 15)
    local offset = FauxScrollFrame_GetOffset(self)
    for visible_line = 1, GLB.MAX_VISIBLE_BIDS do
	local row_widget_name = manage_bids_frame:GetName().."Row"..visible_line.."Button"
	local line = visible_line + offset
	if line <= #bids then
	    local bid = bids[line]
	    local color
	    if bid.won then
		color = BID_WON_COLOR
	    elseif bid.level == "Tell" or bid.whispered then
		color = BID_TELL_COLOR
	    elseif not tonumber(bid.points) then
		color = BID_UNCERTAIN_COLOR
	    else
		color = BID_NORMAL_COLOR
	    end
	    local widget = getglobal(row_widget_name.."Level")
	    widget:SetText(bid.level)
	    widget:SetTextColor(unpack(color))
	    widget = getglobal(row_widget_name.."Points")
	    widget:SetText(bid.points)
	    widget:SetTextColor(unpack(color))
	    widget = getglobal(row_widget_name.."TotalPoints")
	    widget:SetText("of "..(bid.total_points or "no"))
	    widget:SetTextColor(unpack(color))
	    widget = getglobal(row_widget_name.."Bidder")
	    widget:SetText(bid.bidder)
	    widget:SetTextColor(unpack(color))
	    getglobal(row_widget_name):Show()
	else
	    getglobal(row_widget_name):Hide()
	end
    end
end


function GLB.ManageBidsFrame_Update(manage_bids_frame)
    local scroll_bar = getglobal(manage_bids_frame:GetName().."BidsScrollBar")
    GLB.BidsFrameManageBidsScrollBar_Update(scroll_bar)
end


function GLB.ManageBidsFrame_OnLoad(self)
    self:SetScale(0.67)
end


function GLB.ManageBidsFrame_OnShow(self)
    GLB.ManageBidsFrame_Update(self)
end


function GLB.ManageBidsFrame_OnHide(self)
    local bids_frame = self:GetParent()
    local item = bids_frame.item
    GLB.Debug("ManageBidsFrame:Hide() for #"..bids_frame:GetParent():GetID())
    -- XXX stop_accepting_bids redundant if awarded, otherwise not
    if item.bid_taker == UnitName("player") then
	if not item.awarded then
	    GLB.AnnounceCloseBidding(item.name)
	end
	GLB.AnnounceBiddingDone(item.name)
    end
end


function GLB.ManageBidsFrame_OnUpdate(self, elapsed)
    local bids_frame = self:GetParent()
    local item = bids_frame.item
    local now = GetTime()
    if item.countdown then
	local timer = getglobal(self:GetName().."Timer")
	local t = max(0, item.countdown - now)
	timer:SetText(string.format("%d:%02d", math.floor(t / 60), t % 60))
	if item.bid_taker == UnitName("player") then
	    local next_warning = GLB.BidsCountdownWarnings[item.next_countdown]
	    if next_warning and t <= next_warning then
		GLB.Alert(string.format("%s is taking bids for %s ending in about %d seconds",
					item.bid_taker, item.link, t + 0.5))
		item.next_countdown = item.next_countdown + 1
	    end
	end
	if t <= 0 then
	    item.countdown = nil
	    GLB.BidsFrame_Update(bids_frame)
	end
    end
end


-- ManualAuction

local old_GetLootRollItemInfo
local old_GetLootRollItemLink
local old_GetLootRollTimeLeft
local old_RollOnLoot
GLB.NextBogusRollID = 100001
GLB.BogusExpiration = { }
GLB.BogusLink = { }

LOOT_ROLL_INELIGIBLE_REASON_GLB_MANUAL_AUCTION = "This item is being manually auctioned."

local function bogus_roll_frame(rollID)
    for i = 1, NUM_GROUP_LOOT_FRAMES do
        local frame = getglobal("GroupLootFrame"..i)
        if frame.rollID == rollID and GLB.BogusLink[rollID] then
            return frame
        end
    end
end

local function my_GetLootRollItemInfo(rollID)
    if rollID and bogus_roll_frame(rollID) then
	local link = GLB.BogusLink[rollID]
	local name, _, rarity, _, _, _, _, _, _, texture = GetItemInfo(link)
	--[[
	GLB.Debug(string.format("GetLootRollItemInfo %s %s %s",
				(name or "none"), (rarity or "none"),
				(texture or "none")))
]]--
	local reason = "_GLB_MANUAL_AUCTION"
	return texture, name, 1, rarity, true, false, false, false, reason, reason, reason
        --local t = "Interface\\Icons\\INV_Misc_Food_14"
        --return t, "Mutton Chop", 1, 1, true
    else
        return old_GetLootRollItemInfo(rollID)
    end
end

local function my_GetLootRollItemLink(rollID)
    if rollID and bogus_roll_frame(rollID) then
	return GLB.BogusLink[rollID]
    else
        return old_GetLootRollItemLink(rollID)
    end
end

local function my_GetLootRollTimeLeft(rollID)
    local loot_frame = bogus_roll_frame(rollID)
    if loot_frame then
        local remaining = GLB.BogusExpiration[rollID] - GetTime()
        if remaining <= 0 and not loot_frame.cancel_loot_roll_sent then
            GroupLootFrame_OnEvent(loot_frame, "CANCEL_LOOT_ROLL", rollID)
            loot_frame.cancel_loot_roll_sent = true
        end
        return remaining
    else
        return old_GetLootRollTimeLeft(rollID)
    end
end

local function my_RollOnLoot(rollID, mode)
    local loot_frame = bogus_roll_frame(rollID)
    if loot_frame then
        GroupLootFrame_OnEvent(loot_frame, "CANCEL_LOOT_ROLL", rollID)
    else
        return old_RollOnLoot(rollID, mode)
    end
end

function GLB.ManualAuction(sender, duration, item_name)
    GLB.Debug(string.format("ManualAuction sender=%s duration=%s item=%s",
			    NN(sender), NN(duration), NN(item_name)))
    if not (GLB.IsOfficial(sender) or sender == UnitName("player")) then
	return
    end
    local name, link = GetItemInfo(item_name)
    if not name then
	GLB.ChatMessage(string.format("The item %s being manually auctioned by %s has not yet been seen by this client.  Please click on a link to the item and try again.",
				      NN(item_name), NN(sender)))
	return
    end
    item_name = name
    local item = GLB.GetItem(item_name)
    item.manual = true
    -- show a BoP epic frame
    if old_GetLootRollItemInfo ~= GetLootRollItemInfo then
        old_GetLootRollItemInfo = GetLootRollItemInfo
        old_GetLootRollItemLink = GetLootRollItemLink
        old_GetLootRollTimeLeft = GetLootRollTimeLeft
        old_RollOnLoot = RollOnLoot
        GetLootRollItemInfo = my_GetLootRollItemInfo
        GetLootRollItemLink = my_GetLootRollItemLink
        GetLootRollTimeLeft = my_GetLootRollTimeLeft
        RollOnLoot = my_RollOnLoot
    end
    GLB.BogusExpiration[GLB.NextBogusRollID] = GetTime() + duration
    GLB.BogusLink[GLB.NextBogusRollID] = link
    GroupLootFrame_OpenNewFrame(GLB.NextBogusRollID, duration)
    GLB.NextBogusRollID = GLB.NextBogusRollID + 1
end


function GLB.AboutToTakeBids(sender, item_name)
    GLB.Debug(string.format("AboutToTakeBids sender=%s item=%s",
			    NN(sender), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    if not item.bid_taker then
	item.bid_taker = sender
	item.awaiting = true
    end
    -- hide my other item TakeBidsFrames
    -- hide other official's TakeBidsFrame(s) for this item
    -- show my AwaitingGoAheadFrame(s) for this item
    -- show other official's BidTakerFrame(s) for this item
    -- hide all ManageBidsFrames
    for bids_frame in GLB.BidsFrames() do
	GLB.BidsFrame_Update(bids_frame)
    end
    GLB.Debug(string.format("AboutToTakeBids SendGoAhead=%s sender=%s", NN(item.bid_taker), NN(sender)))
    if item.bid_taker == sender then
	GLB.SendGoAhead(item.bid_taker, item.name)
    end
end


function GLB.GoAhead(sender, item_name)
    GLB.Debug(string.format("GoAhead sender=%s item=%s",
			    NN(sender), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    if item.bid_taker == UnitName("player") then
	item.go_aheads[sender] = true
	if GLB.BidTakingBlockingOfficial(item) then
	    GLB.UpdateFrames(item.name)
	else
	    GLB.AnnounceTakingBids(item.name)
	end
    end
end


function GLB.TakingBids(sender, item_name)
    GLB.Debug(string.format("TakingBids sender=%s item=%s",
			    NN(sender), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    -- HACK should already be set... but it could have "two generals"
    item.bid_taker = sender
    item.awaiting = false
    item.awarded = false
    item.countdown = GetTime() + max(GLB.BidsCountdownStart,
				     GLB.BidsCountdownWarnings[1])
    item.last_countdown_warning = 0
    if item.bid_taker == UnitName("player") then
	local count = count_frames_with_item(item.name)
	local copies = count > 1 and string.format("%d copies of ", count) or ""
	GLB.Alert(string.format("%s is taking bids for %s%s",
				UnitName("player"), copies, item.link))
	GLB.MostRecentItem = item.name
	-- XXX redundant
	for bids_frame in GLB.BidsFrames(item.name) do
	    -- XXX rewrite to avoid globals
	    local manage_bids_frame = getglobal(bids_frame:GetName().."ManageBidsFrame")
	    GLB.BidsFrame_WipeBids(bids_frame)
	    table.wipe(item.whispers)
	end
    end
    for bids_frame in GLB.BidsFrames() do
	GLB.BidsFrame_Update(bids_frame)
    end
end


function GLB.PlaceBid(sender, level, item_name, points)
    -- directly from bidder's addon
    GLB.Debug(string.format("PlaceBid sender=%s item=%s level=%s points=%s",
			    NN(sender), NN(item_name), NN(level), NN(points)))
    local item = GLB.GetItem(item_name)
    local total_points = GLB.GetPoints(sender) or 0
    level = GLB.ConstrainLevel(sender, level)
    if not points then
	points = GLB.EstimatePoints(sender, level)
    end
    if not points then
	points = GLB.MinimumBidPoints
    end
    GLB.SendAcceptBid(level, item.name, sender, points)
    -- XXX whisper only?
    if sender ~= UnitName("player") then
	-- XXX having two bid takers automatically reply to whispers can cause an infinite loop
	local msg = string.format("%s bid (%s of %s points) accepted for %s",
				  level, points, (total_points or "no"),
				  item.link)
	GLB.MaybeWhisper(sender, msg)
    end
    -- if someone else has (more recent) data that bid taker lacks, find out and recalculate later.
    GLB.SendPointsRequest(sender)
    GLB.AnnounceReceiveBid(sender, level, points, item.name)
end


function GLB.AnnounceReceiveBid(bidder, level, points, item_name)
    -- sends only to passed officials
    local me = UnitName("player")
    local item = GLB.GetItem(item_name)
    -- HACK whether i've passed or not, i need the bid info
    GLB.ReceiveBid(me, bidder, level, points, item.name)
    for official in GLB.PassedOfficials(item) do
	if official ~= me then
	    GLB.SendReceiveBid(official, bidder, level, points, item.name)
	end
    end
end


function GLB.ReceiveBid(sender, bidder, level, points, item_name)
    GLB.Debug(string.format("ReceiveBid sender=%s bidder=%s level=%s points=%s item=%s",
			    NN(sender),
			    NN(bidder), NN(level), NN(points), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    for bids_frame in GLB.BidsFrames(item.name) do
	local total_points = GLB.GetPoints(bidder)
	if level then
	    GLB.BidsFrame_SetBid(bids_frame, level, points, bidder, total_points)
	else
	    GLB.BidsFrame_DeleteBid(bids_frame, bidder)
	end
    end
end


function GLB.AnnounceComment(bidder, item_name, comment)
    local me = UnitName("player")
    local item = GLB.GetItem(item_name)
    GLB.Comment(me, bidder, item.name, comment)
    for official in GLB.PassedOfficials(item) do
	if official ~= me then
	    GLB.SendComment(official, bidder, item.name, comment)
	end
    end
end


function GLB.Comment(sender, bidder, item_name, comment)
    GLB.Debug(string.format("Comment sender=%s item=%s bidder=%s comment=%s",
			    NN(sender), NN(item_name), NN(bidder), NN(comment)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    item.whispers[bidder] = (item.whispers[bidder] or "").."\n"..comment
    -- XXX tell visible tooltip to update?
    local _, bid = GLB.GetBidByName(item, bidder)
    if bid then
	bid.whispered = true
	GLB.UpdateFrames()
    else
	for bids_frame in GLB.BidsFrames(item.name) do
	    local level, points = "Tell", "?"
	    local total_points = GLB.GetPoints(bidder)
	    -- XXX rewrite to bypass bids_frame
	    GLB.BidsFrame_SetBid(bids_frame, level, points, bidder, total_points)
	end
    end
end


function GLB.AnnounceCancelBid(bidder, item_name)
    local me = UnitName("player")
    local item = GLB.GetItem(item_name)
    GLB.CancelBid(me, bidder, item.name)
    GLB.SendAcceptBid("cancel", item.name, bidder, 0)
    for official in GLB.PassedOfficials(item) do
	if official ~= me then
	    GLB.SendCancelBid(official, bidder, item.name)
	end
    end
end


function GLB.CancelBid(sender, bidder, item_name)
    GLB.Debug(string.format("CancelBid sender=%s item=%s bidder=%s",
			    NN(sender), NN(item_name), NN(bidder)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    GLB.DeleteBid(item, bidder)
    GLB.UpdateFrames(item.name)
end


function GLB.Disenchant(sender, item_name)
    GLB.Debug(string.format("Disenchant sender=%s item=%s",
			    NN(sender), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    if item.bid_taker == UnitName("player") then
	GLB.Alert("Disenchanting "..item.link)
    end
    -- HACK zero is a true value
    item.awarded = 0
    GLB.UpdateFrames(item.name)
end


-- XXX should reorder args
function GLB.Award(when, sender, winner, level, points, item_name, link)
    -- called for each awardee
    GLB.Debug(string.format("Award when=%s sender=%s item=%s winner=%s level=%s points=%s",
			    NN(when),
			    NN(sender), NN(item_name),
			    NN(winner), NN(level), NN(points)))
    if not GLB.IsOfficial(sender) then return end
    local item = GLB.GetItem(item_name)
    item.awarded = true
    local _, bid = GLB.GetBidByName(item, winner)
    if bid then
	bid.won = true
	GLB.UpdateFrames(item.name)
    end
    -- written this way to not require an active auction
    if sender == UnitName("player") then
	local win_info = string.format("%s (%s bid for %s points)",
				       link, level, points)
	GLB.Alert(winner.." wins "..win_info)
	SendChatMessage(GLB.WhisperPrefix.."You won "..win_info, "WHISPER", nil, winner)
	GLB.AdjustPoints(winner, -points)
	local points, mtime, routing = GLB.GetPoints(winner)
	GLB.SendPointsUpdate(winner, points, mtime, routing)
    end
end


function GLB.BiddingDone(sender, item_name)
    GLB.Debug(string.format("BiddingDone sender=%s item=%s",
			    NN(sender), NN(item_name)))
    if not GLB.IsOfficial(sender) then return end
    local all_done = true
    local item = GLB.GetItem(item_name)
    item.done = true
    for bids_frame in GLB.BidsFrames(item.name) do
	-- HACK
	if bids_frame:IsShown() then
	    all_done = false
	else
	    bids_frame.item = nil
	end
    end
    for bids_frame in GLB.BidsFrames() do
	GLB.BidsFrame_Update(bids_frame)
    end
    -- XXX edge case of a dup item appearing at same instant
    -- XXX redundant as frame hide does this
    if all_done then
	GLB.ClearItem(item.name)
    end
end


function GLB.PointsUpdate(sender, bidder, points, mtime, routing)
    GLB.Debug(string.format("PointsUpdate sender=%s bidder=%s",
			    NN(sender), NN(bidder)))
    if not GLB.IsOfficial(sender) then return end
    -- XXX no assurance that the points have been updated yet
    for bids_frame in GLB.BidsFrames() do
	local item = bids_frame.item
	if item and item.bids and not item.awarded then
	    GLB.RecalculateBid(item, bidder)
	end
    end
    GLB.UpdateFrames()
end


function GLB.Pool(sender, pool)
    GLB.Debug(string.format("Pool sender=%s pool=%s",
			    NN(sender), NN(pool)))
    if not GLB.IsOfficial(sender) then return end
    for bids_frame in GLB.BidsFrames() do
	local item = bids_frame.item
	if item and item.bids then
	    -- HACK as we modify item.bids
	    local bidders = { }
	    for _, bid in pairs(item.bids) do
		bidders[bid.bidder] = true
	    end
	    for bidder in pairs(bidders) do
		GLB.RecalculateBid(item, bidder)
	    end
	end
    end
    GLB.UpdateFrames()
end


GLB.BidsFrame_AddonMessageHandlers = {
    [GLB.DecodeManualAuction] = GLB.ManualAuction,
    [GLB.DecodeAboutToTakeBids] = GLB.AboutToTakeBids,
    [GLB.DecodeGoAhead] = GLB.GoAhead,
    [GLB.DecodeTakingBids] = GLB.TakingBids,
    [GLB.DecodePlaceBid] = GLB.PlaceBid,
    [GLB.DecodeReceiveBid] = GLB.ReceiveBid,
    [GLB.DecodeComment] = GLB.Comment,
    [GLB.DecodeCancelBid] = GLB.CancelBid,
    [GLB.DecodeCloseBidding] = false,
    [GLB.DecodeDisenchant] = GLB.Disenchant,
    [GLB.DecodeAward] = GLB.Award,
    [GLB.DecodeBiddingDone] = GLB.BiddingDone,
    [GLB.DecodePointsUpdate] = GLB.PointsUpdate,
    [GLB.DecodePool] = GLB.Pool,
    [GLB.DecodeAltUpdate] = false,
    [GLB.DecodePointsRequest] = false,
}


function GLB.SharedBidsFrame_OnLoad(self)
    GLB.ResetItems()
    self:RegisterEvent("CHAT_MSG_ADDON")
    self:RegisterEvent("CHAT_MSG_WHISPER")
    self:RegisterEvent("CHAT_MSG_LOOT")
end


function GLB.SharedBidsFrame_OnUnload(self)
    self:UnregisterEvent("CHAT_MSG_ADDON")
    self:UnregisterEvent("CHAT_MSG_WHISPER")
    self:UnregisterEvent("CHAT_MSG_LOOT")
    GLB.ResetItems()
end


local args = { }

function GLB.SharedBidsFrame_OnEvent(self, event, ...)
    if event == "CHAT_MSG_ADDON" then
	local addon, msg, _, sender = ...
	if addon == GLB.AddonIdent then
	    -- XXX may want to optimize parser/side_effect to only have one retval/argument
	    -- XXX also may want to genericize parsing
	    local handled = false
	    for parser_fn, side_effect_fn in pairs(GLB.BidsFrame_AddonMessageHandlers) do
		args = { parser_fn(msg, sender) }
		if #args > 0 then
		    handled = true
		    if side_effect_fn then
			side_effect_fn(sender, unpack(args))
		    end
		    break
		end
	    end
	    if not handled then
		GLB.Debug("unrecognized message "..msg.." from "..sender)
	    end
	end
    elseif event == "CHAT_MSG_WHISPER" then
	local msg, sender = ...
	local active = UnitInRaid(sender) or UnitInParty(sender) or sender == UnitName("player")
	local item = GLB.GetActiveItem()
	if item and active then
	    local function record_whisper()
		GLB.AnnounceComment(sender, item.name, msg)
	    end
	    local function place_bid(level)
		record_whisper()
		GLB.PlaceBid(sender, level, item.name)
	    end
	    local function cancel_bid()
		record_whisper()
		GLB.AnnounceCancelBid(sender, item.name)
	    end
	    local function antide_deprecated()
		record_whisper()
		if sender ~= UnitName("player") then
		    GLB.MaybeWhisper(sender, GLB.AntidisenchantWarning)
		end
	    end
	    local function reply_with_versus()
		local vs = { }
		local function maybe_add_player(name)
		    if name ~= nil then
			local _, unit_class = UnitClass(name)
			if GLB.IsEquippableItemBy(item.link, unit_class) then
			    table.insert(vs, { points = GLB.GetPoints(name),
					       name = name })
			end
		    end
		end
		if GetNumRaidMembers() > 0 then
		    for i = 1, MAX_RAID_MEMBERS do
			maybe_add_player(GetRaidRosterInfo(i))
		    end
		else
		    maybe_add_player(UnitName("player"))
		    for i = 1, MAX_PARTY_MEMBERS do
			maybe_add_player(UnitName("party"..i))
		    end
		end
		table.sort(vs, function(a, b)
				   return (a.points or 0) > (b.points or 0)
			       end)
		if #vs > 0 then
		    local reply = string.format("Eligible for %s:", item.link)
		    SendChatMessage(GLB.WhisperPrefix..reply,
				    "WHISPER", nil, sender)
		    for _, opponent in ipairs(vs) do
			local reply = string.format("     %d %s",
						    (opponent.points or 0),
						    opponent.name)
			SendChatMessage(GLB.WhisperPrefix..reply,
					"WHISPER", nil, sender)
		    end
		else
		    local reply = string.format("Nobody is eligible for %s",
						item.link)
		    SendChatMessage(GLB.WhisperPrefix..reply,
				    "WHISPER", nil, sender)
		end
	    end
	    local function instructions()
		record_whisper()
		if bidder ~= UnitName("player") then
		    GLB.MaybeWhisper(sender, GLB.BidInstructions)
		end
	    end
	    local function place_full_bid() place_bid("Full") end
	    local function place_half_bid() place_bid("Half") end
	    local function place_5pt_bid() place_bid("5pt.") end
	    local whisper_patterns = {
		["^[fF][uU][lL][lL]$"] = place_full_bid,
		["^[bB][iI][gG]$"] = place_full_bid,
		["^[tT][hH][iI][rR][dD]$"] = place_full_bid,
		["^[hH][aA][lL][fF]$"] = place_half_bid,
		["^[sS][mM][aA][lL][lL]$"] = place_half_bid,
		["^[lL][iI][tT][tT][lL][eE]$"] = place_half_bid,
		["^[sS][iI][xX][tT][hH]$"] = place_half_bid,
		["^[aA][nN][tT][iI]$"] = antide_deprecated,
		["^[aA][nN][tT][iI][- ]?[dD][eE]$"] = antide_deprecated,
		["^[fF][iI][vV][eE]$"] = place_5pt_bid,
		["^5$"] = place_5pt_bid,
		["^5%s*[pP][tT][sS]?%.?$"] = place_5pt_bid,
		["^5%s*[pP][oO][iI][nN][tT][sS]?$"] = place_5pt_bid,
		["^[mM][iI][nN]$"] = place_5pt_bid,
		["^[mM][iI][nN][iI][mM][uU][mM]$"] = place_5pt_bid,
		["^[cC][aA][nN][cC][eE][lL]$"] = cancel_bid,
		["^[vV][eE][rR][sS][uU][sS]$"] = reply_with_versus,
		["^[dD][kK][pP]$"] = false,
		["^[pP][oO][iI][nN][tT][sS]$"] = false,
		["^[wW][hH][oO][mM]?$"] = false,
		["^[aA][uU][dD][iI][tT]$"] = false,
		["^"..GLB.WhisperPrefix] = function() end,
		["^[hH][eE][lL][pP]$"] = instructions,
	    }
	    local found = false
	    for pattern, fn in pairs(whisper_patterns) do
		if string.match(msg, pattern) then
		    if fn then
			fn()
		    end
		    found = true
		    break
		end
	    end
	    if not found then
		instructions()
	    end
	end
    elseif event == "CHAT_MSG_LOOT" then
	-- warning: requires Interface > Display > Detailed Loot Information
	-- fake it with:
	-- /script GroupLootBidder.SharedBidsFrame_OnEvent(nil, "CHAT_MSG_LOOT", string.format(LOOT_ROLL_PASSED, player, link))
	-- /script GroupLootBidder.SharedBidsFrame_OnEvent(nil, "CHAT_MSG_LOOT", string.format(LOOT_ROLL_PASSED, "Lautrec", "|cffffffff|Hitem:3770:0:0:0:0:0:0:1733319661:31|h[Mutton Chop]|h|r"))
	local msg = ...
	--GLB.Debug("BidsFrame loot msg="..msg)
	local function ignore()
	end
	local function player_passed(name, link)
	    --GLB.Debug("BidsFrame loot passed by "..name)
	    if name and link then
		local me = UnitName("player")
		local item_name = GetItemInfo(link)
		local item = GLB.GetItem(item_name)
		item.passed[name] = (item.passed[name] or 0) + 1
		if (item.passed[name] >= count_frames_with_item(item.name)
		    and item.bid_taker == me
	            and name ~= me
		    and GLB.IsOfficial(name)) then
		    GLB.SendBiddingState(item, name)
		end
	    end
	end
	local function you_passed(link)
	    -- if i passed on it, i might want to monitor it
	    --GLB.Debug("BidsFrame loot you passed on "..link)
	    player_passed(UnitName("player"), link)
	    GLB.UpdateFrames(item_name)
	end
	local function looted(name, link)
	    --GLB.Debug(string.format("Bids Frame loot %s looted %s", oname, link))
	end
	local function you_looted(link)
	    looted(UnitName("player"), link)
	end
	local loot_patterns = {
	    { format_to_pattern(LOOT_ROLL_ALL_PASSED), ignore },
	    { format_to_pattern(LOOT_ROLL_PASSED_SELF), you_passed },
	    { format_to_pattern(LOOT_ROLL_PASSED), player_passed },
	    { format_to_pattern(LOOT_ROLL_PASSED_AUTO), player_passed },
	    { format_to_pattern(LOOT_ROLL_PASSED_AUTO_FEMALE), player_passed },
	    { format_to_pattern(LOOT_ITEM_SELF), you_looted },
	    { format_to_pattern(LOOT_ITEM), looted },
	}
	for _, pf in ipairs(loot_patterns) do
	    local pattern, fn = unpack(pf)
	    local args = { string.find(msg, pattern) }
	    if args[1] then
		fn(select(3, unpack(args)))
		break
	    end
	end
    end
end


function GLB.SendBiddingState(item, recipient)
    -- resend backlogue
    -- slight bug: if bid follows a comment, the spectator's bids shows up as "red" as that's determined by the order of bids vs. comments
    for _, bid in pairs(item.bids) do
	GLB.SendReceiveBid(recipient, bid.bidder, bid.level,
			   bid.points, item.name)
    end
    for bidder, whispers in pairs(item.whispers) do
	-- XXX note does not split the aggregated whispers
	GLB.SendComment(recipient, bidder, item.name, whispers)
    end
end


function GLB.SharedBidsFrame_OnUpdate(self, elapsed)
    local now = GetTime()
end


-- /print GroupLootFrame1BidsFrameManageBidsFrameBidsScrollBar
-- /script GroupLootBidder.BidsFrame_SetBid(GroupLootFrame1BidsFrame, "Full", 10, "test"..math.ceil(random() * 100))
