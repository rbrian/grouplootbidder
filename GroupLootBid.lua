-- Copyright (c) 2009-2011, r. brian harrison.  All rights reserved.


-- aliases

local GLB = GroupLootBidder


-- utilities


local function loot_frame_get_item(loot_frame)
    if loot_frame.rollID then
	local _, item_name = GetLootRollItemInfo(loot_frame.rollID)
	local item_link = GetLootRollItemLink(loot_frame.rollID)
	return item_name, item_link
    else
	return nil
    end
end


function GLB.BidFrame_GetItem(bid_frame)
    return loot_frame_get_item(bid_frame:GetParent())
end


-- widget handlers


StaticPopupDialogs["GROUP_LOOT_BID_NOT_AWARDED"] = {
    text = "Are you sure you want to roll %s?  The item has not been awarded to you (yet).",
    button1 = OKAY,
    button2 = CANCEL,
    OnAccept = function(self, data)
		   RollOnLoot(data.rollID, data.roll)
	       end,
    timeout = 0,
    exclusive = 1,
    whileDead = 1,
    hideOnEscape = 1,
    hasItemFrame = 1,
}



local function roll_on_loot_or_confirm(self, roll_name, roll)
    local loot_frame = self:GetParent()
    local bid_frame = getglobal(loot_frame:GetName().."BidFrame")
    local rollID = loot_frame.rollID
    if bid_frame:IsShown() then
	local texture, name, count, quality = GetLootRollItemInfo(rollID)
	local color = ITEM_QUALITY_COLORS[quality]
	local data = { rollID = rollID, roll = roll, link = link, texture = texture, color = { color.r, color.g, color.b }, name = name, count = count }
	local dialog = StaticPopup_Show("GROUP_LOOT_BID_NOT_AWARDED", roll_name, nil, data)
	if dialog then
	    dialog.data = data
	end
    else
	RollOnLoot(self:GetParent().rollID, roll)
    end
end



function GLB.RollButton_OnClick(self, button, down)
    GLB.Debug("RollButton:Click() for #"..self:GetParent():GetID())
    roll_on_loot_or_confirm(self, "Need", 1)
end



function GLB.GreedButton_OnClick(self, button, down)
    GLB.Debug("GreedButton:Click() for #"..self:GetParent():GetID())
    roll_on_loot_or_confirm(self, "Greed", 2)
end


function GLB.DisenchantButton_OnClick(self, button, down)
    GLB.Debug("DisenchantButton:Click() for #"..self:GetParent():GetID())
    roll_on_loot_or_confirm(self, "Disenchant", 3)
end


local function bid_check_hide(self)
    getglobal(self:GetName().."FullBidCheck"):Hide()
    getglobal(self:GetName().."HalfBidCheck"):Hide()
    getglobal(self:GetName().."FivePointBidCheck"):Hide()
end

function GLB.BidFrame_OnLoad(self)
    -- in OnLoad because an addon message triggers Show()
    self:RegisterEvent("CHAT_MSG_ADDON")
    GLB.Debug("BidFrame:Load() for #"..self:GetParent():GetID())
    if GLB.RollWarningWhileBidding then
	getglobal(self:GetParent():GetName().."RollButton"):SetScript("OnClick", GLB.RollButton_OnClick)
	getglobal(self:GetParent():GetName().."GreedButton"):SetScript("OnClick", GLB.GreedButton_OnClick)
	getglobal(self:GetParent():GetName().."DisenchantButton"):SetScript("OnClick", GLB.DisenchantButton_OnClick)
    end
    -- HACK
    local function fix_anchors(level)
	local bid = getglobal(self:GetName()..level.."BidButton")
	local check = getglobal(self:GetName()..level.."BidCheck")
	check:ClearAllPoints()
	check:SetPoint("LEFT", bid, "RIGHT", 1, 0)
    end
    fix_anchors("Full")
    fix_anchors("Half")
    fix_anchors("FivePoint")
end

function GLB.BidFrame_OnShow(self)
    local loot_frame = self:GetParent()
    GLB.Debug("BidFrame:Show() for #"..loot_frame:GetID())
    if false then
	local stack = debugstack(1, 0, 10)
	for i, line in ipairs({ string.split("\n", stack) }) do
	    if line ~= "" then
		GLB.Debug("BidFrame stack @"..i..": "..line)
	    end
	end
    end
    -- hack:
    if loot_frame.rollID ~= self.shown_rollID then
	GLB.ChatMessage("Warning: BidFrame:Show() bug detected.")
	GLB.Debug("BidFrame:Show() bug detected.")
	self:Hide()
	return
    end
    -- make frame wider
    loot_frame:SetWidth(loot_frame:GetWidth() + self:GetWidth())
    GLB.SendPointsRequest(UnitName("player"))
end

function GLB.BidFrame_OnHide(self)
    local loot_frame = self:GetParent()
    GLB.Debug("BidFrame:Hide() for #"..loot_frame:GetID())
    -- make frame smaller
    bid_check_hide(self)
    loot_frame:SetWidth(loot_frame:GetWidth() - self:GetWidth())
end

function GLB.BidFrame_OnEvent(self, event, ...)
    if event == "CHAT_MSG_ADDON" then
	local addon, msg, _, sender = ...
	if addon == GLB.AddonIdent then
	    -- only accept official communication
	    if GLB.IsOfficial(sender) then
		local loot_frame = self:GetParent()
		local frame_item, frame_link = GLB.BidFrame_GetItem(self)
		local item = GLB.DecodeTakingBids(msg)
		if item and item == frame_item and loot_frame:IsShown() then
		    GLB.Debug("BidFrame AnnounceTakingBids for #"..loot_frame:GetID().." by "..sender.." for "..frame_link)
		    self.bid_taker = sender
		    if sender == UnitName("player") then
			-- XXX redundant with GroupLootFrame_OnHide()?
			self:Hide()
		    else
			-- XXX hack:
			self.shown_rollID = loot_frame.rollID
			self:Show()
			-- XXX hide traditional loot buttons?
		    end
		end
		if self:IsShown() then
		    local level, item = GLB.DecodeAcceptBid(msg)
		    if level and item == frame_item then
			--GLB.ChatMessage(level.." bid accepted for "..frame_link..".")
			-- hack
			if level == "5pt." then level = "FivePoint" end
			local check_tex = getglobal(self:GetName()..level.."BidCheck")
			bid_check_hide(self)
			if check_tex then
			    -- hiding here is largely redundant... unless there's multiple of same item
			    check_tex:Show()
			end
		    end
		    local item = GLB.DecodeCloseBidding(msg)
		    if item and item == frame_item then
			self:Hide()
		    end
		end
	    end
	end
    end
end



local function bid_button_on_click(self, level)
    local bid_frame = self:GetParent()
    local bid_taker = bid_frame.bid_taker
    local item = GLB.BidFrame_GetItem(bid_frame)
    bid_check_hide(bid_frame)
    GLB.SendPlaceBid(level, item, bid_taker)
end

function bid_button_on_enter(self, tooltip, points, bid_taker)
    if points ~= nil then
	tooltip = string.format("%s\n(%d points)\n%s is taking bids.",
				tooltip, points, bid_taker)
    end
    GameTooltip:SetOwner(self, "ANCHOR_RIGHT")
    GameTooltip:SetText(tooltip)
end



function GLB.BidFrameFullBidButton_OnClick(self, button, down)
    bid_button_on_click(self, "Full")
end

function GLB.BidFrameFullBidButton_OnEnter(self, motion)
    local tooltip = "Full bid\nFor primary gear."
    local points = GLB.GetPoints(UnitName("player"))
    local bid_points = GLB.FullPoints(points)
    local bid_taker = self:GetParent().bid_taker
    bid_button_on_enter(self, tooltip, bid_points, bid_taker)

end

function GLB.BidFrameFullBidButton_OnLeave(self)
    GameTooltip:Hide()
end



function GLB.BidFrameHalfBidButton_OnClick(self, button, down)
    bid_button_on_click(self, "Half")
end

function GLB.BidFrameHalfBidButton_OnEnter(self, motion)
    local tooltip = "Half bid\nFor off-spec or backup gear.\nAny Full bid trumps this bid."
    local points = GLB.GetPoints(UnitName("player"))
    local bid_points = GLB.HalfPoints(points)
    local bid_taker = self:GetParent().bid_taker
    bid_button_on_enter(self, tooltip, bid_points, bid_taker)
end

function GLB.BidFrameHalfBidButton_OnLeave(self)
    GameTooltip:Hide()
end



function GLB.BidFrameFivePointBidButton_OnClick(self, button, down)
    bid_button_on_click(self, "5pt.")
end

function GLB.BidFrameFivePointBidButton_OnEnter(self, motion)
    local tooltip = "Five-point bid\nFor preventing unnnecessary disenchanting.\nAny Full or Half bid trumps this bid."
    local bid_taker = self:GetParent().bid_taker
    bid_button_on_enter(self, tooltip, 5, bid_taker)
end

function GLB.BidFrameFivePointBidButton_OnLeave(self)
    GameTooltip:Hide()
end
