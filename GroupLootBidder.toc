## Interface: 42000
## Title: Group Loot Bidder |cff00aa000.17.9|r
## Notes: Bid for phat loots
## Author: r. brian harrison
## Version: 0.17.9
## SavedVariables: GroupLootBidder_PointPool
## SavedVariables: GroupLootBidder_PointsDB
## SavedVariables: GroupLootBidder_PointsDB_mtime
## SavedVariables: GroupLootBidder_PointsDB_routing
## SavedVariables: GroupLootBidder_AwardDB
## SavedVariables: GroupLootBidder_RaidDB
## SavedVariables: GroupLootBidder_AltDB
## SavedVariables: GroupLootBidder_AltDB_mtime
## SavedVariables: GroupLootBidder_AltDB_routing
## SavedVariables: GroupLootBidder_Debugging
## SavedVariables: GroupLootBidder_DebugLog
## SavedVariables: GroupLootBidder_CurrentRaidName

GroupLootBidderVersion.lua
GroupLootBidder.lua
GroupLootBidder.xml
GroupLootBid.lua
GroupLootBid.xml
GroupLootBids.lua
GroupLootBids.xml
GroupLootBidderCLI.lua

