# Copyright (c) 2009-2011, r. brian harrison.  All rights reserved.

VERSION=0.17.9

APP=GroupLootBidder

FILES=	$(APP)/docs/design \
	$(APP)/docs/officers_guide \
	$(APP)/docs/users_guide \
	$(APP)/GroupLootBidder.toc \
	$(APP)/GroupLootBidder.lua \
	$(APP)/GroupLootBidder.xml \
	$(APP)/GroupLootBid.lua \
	$(APP)/GroupLootBid.xml \
	$(APP)/GroupLootBids.lua \
	$(APP)/GroupLootBids.xml \
	$(APP)/GroupLootBidderCLI.lua \
	$(APP)/$(APP)Version.lua

DIRS=	

build:
	sed -i.bak -e "s/^\(## Title: [^|]* |cff00aa00\)[^|]*|r/\1$(VERSION)|r/" -e "s/^\(## Version: \)[0-9.-]*/\1$(VERSION)/" $(APP).toc
	(echo "$(APP) = $(APP) or { }"; \
	 echo "$(APP).VERSION = \"$(VERSION)\"" ) > $(APP)Version.lua
	-rm ../$(APP)-$(VERSION).zip
	(cd .. && zip -r $(APP)-$(VERSION).zip $(FILES) $(DIRS))
